\documentclass{amsart}   	% use "amsart" instead of "article" for AMSLaTeX format
\usepackage{fullpage}
\usepackage{amsmath, amssymb} 
\usepackage{xcolor}
\usepackage{tcolorbox}

\newcommand{\tbox}[3]{
\begin{center}
\begin{tcolorbox}[title=#1,width=#3,coltext=red,colback=green!15!white,colframe=blue!80!black]
#2
\end{tcolorbox}
\end{center}
}

\newcommand{\ibox}[2]{
\begin{center}
\begin{tcolorbox}[width=#2,coltext=red,colback=green!15!white,colframe=blue!80!black]
#1
\end{tcolorbox}
\end{center}
}
\usepackage{draftwatermark}

\SetWatermarkText{SIVARAM}
\SetWatermarkLightness{0.975}
\SetWatermarkScale{4}

\title{Number theory notes from Landau's Elementary number theory}
\author{Sivaram Ambikasaran}
\date{}							% Activate to display a given date or no date

\begin{document}
\maketitle
\hrulefill
%Section: Introduction
\section{Introduction}
\hrulefill

Skipping the first three chapters. The key take home points is provided below.

\begin{enumerate}
\item
\textbf{Fundamental theorem of arithmetic}
\tbox{\textbf{Fundamental theorem of arithmetic}}{Every positive integer can be uniquely represented as a product of prime powers (up-to reordering of prime powers).}{6in}
\item
\textbf{Bezout's lemma}
\tbox{\textbf{Bezout's lemma}}{If $d = \gcd(a,b)$, then there exists integers $x,y$ such that $ax+by = d$. Equivalently, $ax+by = c$ has integer solutions, if and only if $ d \mid c$}{6in}
\item
\textbf{Highest power of prime dividing a factorial}
\tbox{\textbf{Highest power of prime dividing a factorial}}{The highest power $s_p(n)$ of prime $p$ dividing $n!$ is given by
$$\boxed{\color{red}{s_p(n) = \sum_{k=1}^{\infty} \left[ \dfrac{n}{p^k}\right]}}$$ It is important to now that the infinite series converges for sure, since the infinite series is actually a finite series, i.e., terms vanish to zero beyond some $k$. Further as a corollary, we also have
$$\boxed{\color{red}{n! = \prod_{p \leq n}p^{s_p(n)}}}$$
}{6in}
\textbf{Proof}:
\begin{enumerate}
\item
\textbf{First proof}: The number of positive multiples of the number $p$ up to $n$ is $\left[\dfrac{n}p\right]$; However, whenever we hit a multiple of $p$, which is also a multiple of $p^2$ we need to count it twice and in general, whenever we hit a multiple of $p$, which is also a multiple of $p^k$, we need to count it $k$ times. Hence, the highest power of $p$ that divides $n!$ is
$$\sum_{k=1}^{\infty} \text{ Number of positive multiples of }p^k\text{ up-to }n = \sum_{k=1}^{\infty} \left[\dfrac{n}{p^k}\right] $$
\item
\textbf{Second proof}: Let us define the Von Mangoldt function $\Lambda(n)$:
$$
\Lambda(n) =
\begin{cases}
\log(p) & \text{ if } n =p^c; c \geq 1\\
0 & \text{ else}
\end{cases}
$$
First note that
$$a_{\ell} \log(p_{\ell}) = \sum_{m=1}^{a_{\ell}} \log(p_{\ell}) = \sum_{m=1}^{a_{\ell}} \Lambda(p_{\ell}^m)$$
Hence, if $n=p_1^{a_1}p_2^{a_2}\cdots p_k^{a_k}$, we then have
$$\log(n) = \sum_{\ell=1}^k a_{\ell} \log(p_{\ell}) = \sum_{\ell=1}^k\sum_{m=1}^{a_{\ell}} \Lambda(p_{\ell}^m) = \sum_{d \mid n} \Lambda(d)$$
We have
\begin{align*}
\log(n!) & = \sum_{a=1}^n \log(a) = \sum_{a=1}^n \sum_{d \mid a} \Lambda(d) = \sum_{d=1}^n \sum_{\overset{a=md}{a \leq n}} \Lambda(d) = \sum_{d=1}^n \left[\dfrac{n}d\right] \Lambda(d)\\
& = \sum_{p \leq n}\log(p) \left[\dfrac{n}p\right] + \sum_{p \leq n}\log(p) \left[\dfrac{n}{p^2}\right] + \cdots + \text{ad inf.}\\
& = \sum_{p \leq n} \left(\sum_{k=1}^{\infty} \left[\dfrac{n}{p^k}\right]\right) \log(p) = \sum_{p \leq n} s_p(n) \log(p)\\
n! & = \prod_{p \leq n} p^{s_p(n)}
\end{align*}
\end{enumerate}
\end{enumerate}

\hrulefill
%Section: Mobius function
\section{Mobius function}
\hrulefill
\ibox{
The Mobius function $\mu(n)$ is defined as
\begin{align*}
\mathbf{
\mu(n) =
\begin{cases}
(-1)^r & \text{ if $n$ is square-free and is of the form }n=p_1p_2\ldots p_r\\
0 & \text{ if $n$ is not square-free}
\end{cases}
}
\end{align*}
}{4.5in}

\hrulefill
\begin{enumerate}
\item
If $(a,b) = 1$, then \ibox{$\mu(ab) = \mu(a) \mu(b)$}{1.5in}
\textbf{Proof}:
\begin{enumerate}
\item
If $a$ and $b$ are square-free, say $a=p_1p_2\cdots p_k$ and $b = p_{k+1}p_{k+2}\cdots p_{k+m}$, we then have $p_i \neq p_j$ for any $i,j \in \{1, 2, \ldots, k, k+1, k+2, \ldots, k+m\}$ (Recall that $\gcd(a,b) = 1$). We have $ab = p_1p_2\cdots p_{k+m}$. We now obtain $\mu(a) = (-1)^k$, $\mu(b) = m$ and $\mu(ab) = (-1)^{k+m}$. Hence, we have $\mu(ab) = \mu(a) \mu(b)$.
\item
If one of $a$ or $b$ is not square-free, i.e., if $\mu(a) = 0$ or $\mu(b) = 0$, we then have $ab$ is also not square-free, i.e., $\mu(ab) = 0$. Hence, again we have $\mu(ab) = \mu(a) \mu(b)$.
\end{enumerate}

\hrulefill
\item
We have \ibox{$\sum_{d \vert n} \mu(d) = \delta_n$}{1.5in}
\textbf{Proof}: If $n=1$, we have $\mu(n) = 1$. Now let us look at $n>1$. If $n=p_1^{a_1}p_2^{a_2}\cdots p_k^{a_k}$, the square-free divisors of $n$ are of the form $p_{i_1}p_{i_2}\cdots p_{i_t}$, where $i_1 ,i_2,\ldots, i_t$ are distinct numbers in the set $\{1,2,\ldots,k\}$. Number of divisors, which are made up of just one prime is $k$. In general, the number of square-free divisors made up of $t$ primes is $\dbinom{k}t$. Hence, we have
$$\sum_{d \vert n} \mu(d) = \sum_{t=0}^k \dbinom{k}t (-1)^t = (1-1)^k = 0$$

\hrulefill
\item
If $\eta \geq 1$, then
\ibox{$\sum_{n=1}^{[ \eta ]} \mu(n) \left[ \dfrac{\eta}n \right] = 1$}{1.75in}
\textbf{Proof}: Note that $\left[\dfrac{\eta}n\right] = \sum_{mn \leq \eta} 1$. Hence, we have
$$\sum_{n=1}^{[ \eta ]} \mu(n) \left[ \dfrac{\eta}n \right] = \sum_{n=1}^{[ \eta ]} \sum_{\overset{m}{mn \leq \eta}} \mu(n) = \sum_{k=1}^{[\eta]} \sum_{d \vert k} \mu(d) = \mu(1) = 1$$

\hrulefill
\item
If $x \geq 1$, we have
\ibox{$\left \vert \sum_{n\leq x} \dfrac{\mu(n)}n\right \vert \leq 1$}{1.5in}
\textbf{Proof}: 
From the previous part, we have
$$1 = \sum_{n \leq x} \mu(n) \left[\dfrac{x}{n}\right] = \sum_{n \leq x} \mu(n) \left(\dfrac{x}{n} - \left\{\dfrac{x}{n}\right\}\right) = x \sum_{n = 1}^x \dfrac{\mu(n)}n - \sum_{n = 1}^x \mu(n)\left\{\dfrac{x}{n}\right\}$$
$$x \sum_{n = 1}^x \dfrac{\mu(n)}n = 1 + \sum_{n = 1}^x \mu(n)\left\{\dfrac{x}{n}\right\} \implies \left \vert x \sum_{n = 1}^x \dfrac{\mu(n)}n \right \vert = \left \vert 1 + \sum_{n = 1}^x \mu(n)\left\{\dfrac{x}{n}\right\} \right \vert \leq 1 + \sum_{n = 1}^x \left \vert \mu(n)\left\{\dfrac{x}{n}\right\} \right \vert \leq 1 + \sum_{n=1}^x 1 = x$$
Hence, we get that $\left \vert \sum_{n\leq x} \dfrac{\mu(n)}n\right \vert \leq 1 \text{ for all }x$.

\hrulefill
\item
Let $F(a)$ be any number-theoretic function and let $G(a) = \sum_{d \mid a} F(d)$. We then have
\tbox{Mobius inversion formula}{$F(a) = \sum_{d \mid a} \mu(d) G(a/d)$}{2in}
\textbf{Proof}: We have
$$G(a/d) = \sum_{b \mid (a/d)} F(b) = \sum_{\overset{b}{bd \mid a}} F(b)$$
Hence, $$\sum_{d \mid a} \mu(d) G(a/d) = \sum_{d \mid a} \mu(d) \sum_{\overset{b}{bd \mid a}} F(b) = \sum_{k \mid a} F\left(\dfrac{a}k \right) \sum_{d \vert k} \mu(d) = \sum_{k \mid a} F\left(\dfrac{a}k \right) \delta_k = F(a)$$
Another way is
$$\sum_{d \mid a} \mu(d) G(a/d) = \sum_{d \mid a} \mu(d) \sum_{\overset{b}{bd \mid a}} F(b) = \sum_{\overset{b,d}{bd \vert a}} \mu(d) F(b) = \sum_{d \mid a} F(d) \sum_{\overset{b}{bd \mid a}} \mu(b) = F(a)$$
\end{enumerate}

\hrulefill
%Section: Euler Toitent function
\section{Euler Toitent function}
\hrulefill
\ibox{
The Euler Toitent function is defined as
$$\phi(n) = \text{Number of numbers less than $n$ and relatively prime to $n$}$$}{6in}

\hrulefill
\begin{enumerate}
\item
First identity:
\ibox{$\sum_{d \mid n} \phi(d) = n$}{1.5in}
\textbf{Proof}: Divide all the numbers from $1$ to $n$ into disjoint classes according to the value of $d = \gcd(a,n)$, i.e., $S_d = \{a \in \{1,2,\ldots,n\}: \gcd(a,n) = d\}$. Note that $\sum_{d \mid n} \vert S_d \vert = n$. We have $\vert S_1 \vert = \phi(n)$, and in general $\vert S_d \vert = \phi(n/d)$. Hence, we have
$$n = \sum_{d \mid n} \phi(n/d) = \sum_{d \mid n} \phi(d)$$

\hrulefill
\item
Second identity:
\ibox{$\phi(n) = n \sum_{d \vert n} \dfrac{\mu(d)}{d}$}{1.75in}
\textbf{Proof}: This follows immediately from the Mobius inversion formula applied to the above result. Since $n = \sum_{d \mid n} \phi(d) \implies \phi(n) = \sum_{d \mid n} \mu(n) \dfrac{n}d = n \sum_{d \mid n} \dfrac{\mu(d)}d$

\hrulefill
\item
$$\boxed{\color{red}{\phi(n) = n \prod_{p \vert n}\left(1-\dfrac1p\right)}}$$
All we need to prove is that
$$\sum_{d \mid n} \dfrac{\mu(d)}d = \prod_{p \vert n}\left(1-\dfrac1p\right)$$
This is true since the only terms that contribute to the sum on the left are divisors that are square-free. The sign to which $\dfrac1d$ appears depends on the number of distinct primes multiplying it, which is given by $\mu(d)$. In general, can be proved by inducting on the number of primes dividing $n$.

\hrulefill
\item
For $n>1$, we have, in the canonical notation, if $n = \prod_{k=1}^r p_k^{a_k}$, then
$$\boxed{\color{red}\phi(n) = \prod_{k=1}^r p_k^{a_k-1}(p_k-1)}$$
\textbf{Proof}: 
This follows immediately from the previous result.

\hrulefill
\item
If $a>0, b>0$ and $\gcd(a,b) = 1$, then $$\boxed{\color{red}{\phi(ab) = \phi(a) \phi(b)}}$$
\textbf{Proof}:
This follows immediately from above.
\end{enumerate}

\hrulefill
\section{Congruences}
%Section: Congruences
\hrulefill

\begin{enumerate}
\item
{
\color{blue}{
$a$ is said to be congruent to $b$ modulo $m$, written as $a \equiv b \pmod{m}$, if $m \mid (a-b)$.
}
}
\item
{
\color{blue}{
$a$ is said to be incongruent to $b$ modulo $m$, written as $a \not\equiv b \pmod{m}$, if $m \nmid (a-b)$.
}
}
\end{enumerate}
Sometimes, we will use the short hand $a \equiv_m b$.

\hrulefill
\begin{enumerate}
\item
Let $f(x) = c_0 + c_1 x + \cdots + c_n x^n$ be any rational integral function with integer coefficients. If $a \equiv_m b$, then $f(a) \equiv_m f(b)$.\\
\textbf{Proof}: This immediately follows form the two facts:
\begin{enumerate}
\item
$a \equiv_m b \implies a^s \equiv_m b^s$.
\item
$a \equiv_m b, c \equiv_m d \implies a+c \equiv_m b+d$.
\end{enumerate}
This justifies the fact that the number of solutions to the congruence $f(x) \equiv_m 0$ is always either $0$ or some finite number ($\leq m$) of solutions $\pmod_m$. For instance, $x^2 \equiv_8 1$ has $4$ solutions $\pmod 8$ given by $\equiv_m 1,3,5,7$.

\hrulefill

\textbf{Complete set of residues}: $0,1,2,\ldots,m-1$ form a complete set of residues.

\textbf{Reduced set of residues}: $a_1,a_2,\ldots,a_{\phi(m)} \in \{1,2,\ldots,m-1\}$ such that $\gcd(a_{\ell},m) = 1$ form a reduced set of residues.

\hrulefill

\item
If $\gcd(k,m) = 1$, then the numbers $0 \cdot k, 1 \cdot k, 2 \cdot k, \cdots, (m-1) \cdot k$ constitute a complete set of residues $\pmod m$. More generally, if $\gcd(k,m) = 1$ and $a_1, a_2, \ldots, a_m$ is any complete set of residues, then so is $a_1k, a_2k, \ldots, a_mk$.\\
\textbf{Proof}: If $a_rk \equiv_m a_sk$, $1 \leq r,s \leq m$, since $\gcd(k,m)=1$ we then have $a_r \equiv_m a_s$. Since $a_1, a_2, \ldots, a_m$ form a complete residue, we have $r = s$. Hence, $a_rk$ are therefore mutually incongruent.

\hrulefill

\item
If $\gcd(k,m) = 1$ and if $a_1,a_2,\ldots,a_{\phi(m)}$ constitute a reduced set of residues $\pmod{m}$, then so do $a_1k, a_2k, \ldots, a_{\phi(m)}k$.\\
\textbf{Proof}: Since $\gcd(a_r,m) = 1$, we have $mx_1 + a_ry_1 = 1$. Similarly, since $\gcd(k, m)=1$, we have $kx_2 + my_2 = 1$. Multiplying the two, we get $$(mx_1 + a_ry_1)(kx_2 + my_2) = 1 \implies m(kx_1x_2 + mx_1y_2 + a_ry_1y_2) + (a_rk)(x_2y_1) = 1$$Hence, we have $\gcd(m, a_rk) = 1$. This means $a_rk \equiv_m a_s$ for some $s \in \{1,2,\ldots,\phi(m)\}$.

\hrulefill
\item
If $f(x) = c_0 + c_1x+ \cdots + c_nx^n$, $p \nmid c_n$ then the congruence $f(x) \equiv_p 0$ has at-most $n$ solutions.\\
\textbf{Proof}:
\begin{itemize}
\item
For $n=0$, if $f(x) = 0$, $p \nmid c_0$, then the congruence $c_0 \equiv_p 0$ has at-most $0$ solutions, which is clearly true.
\item
Let $n>0$ and assume the theorem true for $n-1$. If for degree $n$, we have $n+1$ (incongruent) distinct roots $x_0, x_1, \ldots, x_n$, then note that
$$f(x) - f(x_0) = \sum_{r=1}^n c_r (x^r-x_0^r) = (x-x_0) g(x)$$
where $g(x) = b_0 + b_1x+ \cdots + b_{n-1}x^{n-1}$, where $b_{n-1} = c_n$, $p \nmid b_{n-1}$. Hence, we have
$$(x_k-x_0) g(x_k) \equiv_p f(x_k) - f(x_0) \equiv_p 0$$
Hence, for $k=1,2,\ldots,n$, since $p$ is a prime and $\vert x_k - x_0 \vert <p$, we have $g(x_k) \equiv_p 0$, contradicting the induction hypothesis.
\end{itemize}



\hrulefill
\item
Let $a>0$, $b>0$ and $\gcd(a,b) = 1$. Let $x$ range over a complete set of residues $\pmod{b}$ and $y$ range over a complete set of residues $\pmod{a}$, then $ax+by$ ranges over a complete set of residues $\pmod{ab}$.\\
\textbf{Proof}: Of the $ab$ numbers obtained as $ax+by$, two of them are equal iff
$$ax_1 + by_1 \equiv_{ab} ax_2 + by_2 \implies ax_1 + by_1 \equiv_{a} ax_2 + by_2 \implies \underbrace{by_1 \equiv_{a} by_2 \implies y_1 \equiv_{a} y_2}_{\text{Since }\gcd(a,b)=1}$$
Similarly, we have
$$ax_1 + by_1 \equiv_{ab} ax_2 + by_2 \implies ax_1 + by_1 \equiv_{b} ax_2 + by_2 \implies \underbrace{ax_1 \equiv_{b} ax_2 \implies x_1 \equiv_b x_2 }_{\text{Since }\gcd(a,b)=1}$$

\hrulefill

\item
Let $a>0$, $b>0$ and $\gcd(a,b) = 1$. Let $x$ range over a reduced set of residues $\pmod{b}$ and $y$ range over a reduced set of residues $\pmod{a}$, then $ax+by$ ranges over a reduced set of residues $\pmod{ab}$.\\
\textbf{Proof}: We need to show that if $\gcd(x,b) = 1 = \gcd(y,a)$, then $\gcd(ax+by,ab) = 1$. Let $p$ be a prime such that $p \mid ab$ and $p \mid (ax+by)$. Since $p$ is a prime, we have $p \mid a$ or $p \mid b$. Without loss of generality, let us assume that $p \mid a$ and $p \nmid b$. We also have that $p \mid (ax+by) \implies p \mid by \implies p \mid y$. Hence, we have $p \mid a$ and $p \mid y$, contradicting the fact that $\gcd(a,y) = 1$. Hence, if $\gcd(x,b) = 1 = \gcd(y,a)$, then $\gcd(ax+by,ab) = 1$.

\hrulefill
\item
\textbf{Euler's theorem}: $\boxed{\mathbf{\color{red}{\textbf{If }\gcd(a,n)=1, \text{ we have }a^{\phi(n)} \equiv_n 1 }}}$\\
\textbf{Proof}: Let $b_1,b_2,\ldots,b_{\phi(n)}$ be a reduced set of residues $\pmod{n}$. Since we have $\gcd(a,n) = 1$, we have $ab_r \equiv_n b_p$ for some $p$. Further, all the $aa_r$ form distinct reduced residues $\pmod{n}$, i.e., $ab_r \equiv_n ab_s$ iff $r=s$, this is again from the fact that $\gcd(a,n) = \gcd(b_r,n) = \gcd(b_s,n) = 1$. Hence, we have
$$\prod_{l=1}^{\phi(m)} (ab_l) \equiv_n \prod_{l=1}^{\phi(m)} b_l \implies \underbrace{a^{\phi(n)}\left(\prod_{l=1}^{\phi(m)} b_l \right) \equiv_n \left(\prod_{l=1}^{\phi(m)} b_l \right) \implies a^{\phi(n)} \equiv_n 1}_{\text{Since }\gcd\left(\left(\prod_{l=1}^{\phi(m)} b_l \right), n\right) = 1}$$

\hrulefill
\item
\textbf{Fermat little theorem}: $\boxed{\color{red}{\mathbf{\textbf{If $p \nmid a$, then $a^{p-1} \equiv_p 1$}}}}$\\
\textbf{Proof}: Follows immediately by taking $n=p$ as a prime and noting that $\phi(p) = p-1$.

\hrulefill
\item
\textbf{Wilson's theorem}: $\boxed{\color{red}{\mathbf{\textbf{If $p$ is a prime, then $(p-1)! \equiv_p -1$}}}}$\\
\textbf{First proof}: For $p=2$ and $p=3$, the statement is obvious. For $p>3$, consider the set of $p-3$ numbers, $S = \{2,3,4,\ldots,p-3,p-2\}$. Note that for each $r$ in $S$, we have $\gcd(r,p) = 1$ and there is exactly one $s$ in $S$ such that $rs \equiv_p 1$. Moreover, $r \neq s$. This is because if $r=s$, we have $r^2 \equiv_p 1 \implies p \mid (r+1)(r-1)$. Hence, $r \equiv_p \pm 1$, which clearly does not belong to $S$. Hence, we have $\prod_{r \in S}r \equiv_p 1$. Now note that $(p-1)! = 1 \cdot (p-1) \cdot \prod_{r \in S}r$. Hence, we have $(p-1)! \equiv_p -1$.\\
\textbf{Second proof}: Let $f(x) = x^{p-1} - 1 - \displaystyle \prod_{k=1}^{p-1} (x-k) = c_0 + c_1x + \cdots + c_{p-2} x^{p-2}$, which is a polynomial of degree $p-2$. This has at-most $p-2$ roots $\pmod{p}$ by one of the previous theorems. However, by Fermat's little theorem, it is easy to check that $x=1,2,\ldots,p-1$ are $(p-1)$ roots of $f(x) \equiv_p 0$. This means $f(x) \equiv_p 0$ for all $x$. Now setting $x=p$, we get $$\displaystyle p^{p-1} - 1 - \prod_{k=1}^{p-1}(p-k) \equiv_p 0 \implies \boxed{(p-1)! + 1\equiv_p 0}$$

\end{enumerate}

\hrulefill
\section{Quadratic residues}
\hrulefill

\noindent
\textbf{Quadratic residue}: If the congruence $x^2 \equiv n \pmod m$ has a solution, then $n$ is called a quadratic residue modulo $m$; otherwise, $n$ is called a quadratic non-residue modulo $m$. It is important to note that $0$, $1$ and other perfect squares are quadratic residues modulo any number.\\
\textbf{Legendre's symbol}: If $p\geq2$ and $p \nmid n$, let
$$
\left(\dfrac{n}{p}\right) =
\begin{cases}
1 & \text{ if $n$ is a quadratic residue }\pmod{p}\\
-1 & \text{ if $n$ is a quadratic non-residue }\pmod{p}\\
\end{cases}
$$
Example: $\left(\dfrac{m^2}p\right) = 1$ for $p\geq2$ and $p \nmid m$, in particular $\left(\dfrac1p\right) = 1$ for all $p \geq 2$.

\hrulefill

\begin{enumerate}
\item
Let $p > 2$. In every reduced set of residues $\pmod{p}$, there are exactly $\dfrac{p-1}2$ numbers $n$ for which $\left(\dfrac{n}p \right) = 1$ and hence exactly $\dfrac{p-1}2$ numbers $n$ for which $\left(\dfrac{n}p\right) = -1$. The first set of $\dfrac{p-1}2$ numbers are represented by the residue classes to which the numbers $1^2,2^2,\ldots,\left(\dfrac{p-1}2\right)^2$ belong.

In particular, therefore: Given $p>2$, there exists an $n$ such that $\left(\dfrac{n}p\right)=-1$.\\
\textbf{Proof}: Consider any $n \in \{1,2,\ldots,p-1\}$. The congruence $x^2 \equiv_p n$ if it is solvable has at least one solution in the interval $0 \leq x \leq p-1$; but by a theorem in the previous chapter, we have that there is at most two solutions in that interval. If $p \nmid n$, then there are two solutions since if $x$ is a solution, so is $p-x$ and $x \neq_p p-x$, since $p$ is odd. We now have $(p-x)^2 \equiv_p (-x)^2 \equiv_p x^2$. Hence, the only numbers to consider are $1^2, 2^2,\ldots, \left(\dfrac{p-1}2\right)^2$. These have to leave distinct residues (if two of these say $x_1$ and $x_2$ leave the same residue, then $p-x_1$ and $p-x_2$, will also leave the same residue, which would imply $4$ solutions, contradicting the theorem proved earlier). Hence, proved.

\hrulefill

\item
\textbf{Euler's criterion}: If $p>2$ and $p \nmid n$, then
$$\boxed{\color{red}{n^{(p-1)/2} \equiv_p \dbinom{n}p}}$$
\emph{Remark}: Note that from Fermat's little theorem, we have $n^{(p-1)/2} \equiv_p \pm1$.\\
\textbf{Proof}:
\begin{itemize}
\item
Let $\left(\dfrac{n}p \right) = 1$, by definition we have $x^2 \equiv_p n$. Hence, by Fermat's little theorem, we have $$n^{(p-1)/2} \equiv_p (x^{2})^{(p-1)/2} \equiv_p x^{p-1} \equiv_p 1$$
\item
We proved previously that there are $(p-1)/2$ quadratic residues for $p$. Hence, all the $(p-1)/2$ residues are roots of $y^{(p-1)/2} \equiv_p 1$. And by an earlier theorem, we have that $y^{(p-1)/2} \equiv_p 1$ can have only at-most $(p-1)/2$ solutions. Hence, the rest of the quadratic non-residues have to satisfy $y^{(p-1)/2} \not\equiv_p 1$. However, by Fermat's little theorem, we have $y^{(p-1)/2} \equiv \pm1 \pmod{p}$ for all $p$. Hence, if $m$ is a quadratic non-residue, we then have $m^{(p-1)/2} \equiv_p -1$.
\end{itemize}

\hrulefill
\item
If $p>2$, $p \nmid n$ and $p \nmid n_k$ for $k=1,2,\ldots,r$, then
$$\boxed{\color{red}{\left(\dfrac{n_1n_2\cdots n_r}p\right) = \left(\dfrac{n_1}p\right) \left(\dfrac{n_2}p\right) \cdots \left(\dfrac{n_r}p\right) }}$$
\textbf{Proof}: Follows immediately from induction and using the Euler's criterion that $n^{(p-1)/2} \equiv_p \left(\dfrac{n}p\right)$.

\hrulefill
\item
\textbf{First lemma of the quadratic reciprocity law}
\tbox{First lemma of the quadratic reciprocity law}{$$\left(\dfrac{-1}p\right) = (-1)^{(p-1)/2} \text{ for }p>2$$}{3.25in}
\textbf{Proof}: Follows immediately from Euler's criteria.

\hrulefill
\item
Let $p>2$ and $p \nmid n$. Consider the $\dfrac{p-1}2$ numbers $n,2n,\ldots,\dfrac{(p-1)n}2$ and determine their remainders $\pmod p$. We obtain $\dfrac{p-1}2$ distinct numbers, which belong to $1,2,\ldots,p-1$. Let $m$ be the number of these remainders, which are $>\dfrac{p}2$. (Note that $m$ can be $0$.) We assert that $\left(\dfrac{n}p \right) = (-1)^m$.
\tbox{Gauss Lemma}{Let $p>2$ and $p \nmid n$. $$S_n = \left\{kn \pmod{p} \mid k \in \left\{1,2,\ldots,\dfrac{p-1}2\right\}\right\}$$ $$T_p = \left\{k \pmod{p} \mid k \in \left\{\dfrac{p+1}2, \dfrac{p+3}2, \ldots, p-1\right\}\right\}$$ If $m = \vert S_n \cap T_p \vert$, then $\left(\dfrac{n}p\right) = (-1)^m$}{5in}
\textbf{Proof}: Let $R_p = \left\{k \pmod{p} \mid k \in \left\{1,2,\ldots,\dfrac{p-1}2\right\}\right\}$. Let $X_p = S_n \cap T_p = \{b_1,b_2,\ldots,b_m\}$ and $Y_p = S_n \cap R_p = \{a_1,a_2,\ldots,a_l\}$. It is important to note that $k_1n \equiv_p k_2n \implies k_1 \equiv_p k_2$, i.e., $S_n$ has $\dfrac{p-1}2$ distinct elements. We now have
\begin{align*}
\prod_{k=1}^l a_k \prod_{j=1}^m b_j \equiv_p n^{(p-1)/2} \left(\dfrac{p-1}2\right)! \tag{$\spadesuit$}
\end{align*}
Furthermore, $a_i + b_j \not\equiv_p 0$ for any $a_i \in Y_p$ and $b_j \in X_p$. Hence, if we look at all $a_i$'s and $p-b_j$'s, these take all values between $1$ and $\dfrac{p-1}2$, so we have
\begin{align*}
\left(\dfrac{p-1}2\right)! \equiv_p \prod_{k=1}^l a_k \prod_{j=1}^m (p-b_j) \equiv_p \prod_{k=1}^l a_k \prod_{j=1}^m (-b_j) \equiv_p (-1)^m\prod_{k=1}^l a_k \prod_{j=1}^m b_j \tag{$\clubsuit$}
\end{align*}
Hence, comparing $(\clubsuit)$ and $(\spadesuit)$, we get
$$
\left(\dfrac{p-1}2\right)! \equiv_p (-1)^m n^{(p-1)/2} \left(\dfrac{p-1}2\right)! \implies n^{(p-1)/2} \equiv_p (-1)^{m}
$$
By Euler's criterion, we have $\left(\dfrac{n}p\right) \equiv_p n^{(p-1)/2} \implies \left(\dfrac{n}p\right) = (-1)^m$

\hrulefill
\item
\textbf{Second lemma of the quadratic reciprocity law}
\tbox{Second lemma of the quadratic reciprocity law}{$$\left(\dfrac{2}p\right) = (-1)^{(p^2-1)/8} \text{ for }p>2$$}{3.25in}
\textbf{Proof}: We repeat the same argument as the previous problem for $n=2$. The numbers we consider are $S_n = \{2\mod{p},4\pmod{p},\ldots,(p-1)\pmod{p}\}$. We just need to find $m$, i.e., number of elements of $S$ greater than $\dfrac{p}2$.
\begin{enumerate}
\item
If $p=4k+1$, the set $S_n$ is $\{2k+2,2k+4,\ldots,4k\}$. Hence, $m=k$.
\item
If $p=4k+3$, the set $S_n$ is $\{2k+2,2k+4,\ldots,4k+2\}$. Hence, $m=k+1$.
\end{enumerate}
Hence, we get that
\begin{align*}
m =
\begin{cases}
\dfrac{p-1}4 & \text{ if }p=4k+1\\
\dfrac{p+1}4 & \text{ if }p=4k+3\\
\end{cases}
\end{align*}
We also have
\begin{align*}
\dfrac{p^2-1}8 \equiv_2
\begin{cases}
\dfrac{p-1}4 & \text{ if }p=4k+1\\
\dfrac{p+1}4 & \text{ if }p=4k+3\\
\end{cases}
\end{align*}
Hence, $\left(\dfrac{2}p\right) = (-1)^{(p^2-1)/8}$.

\hrulefill
\item
\textbf{Eisenstein Lemma}:
\tbox{Eisenstein's lemma}{If $p$ and $q$ are distinct odd primes, we then have$$\left(\dfrac{q}p\right) = (-1)^{\displaystyle\sum_{k=1}^{(p-1)/2} \left[\dfrac{kq}p\right]}$$}{3.25in}

\textbf{Proof}: Consider the numbers $kq$, where $1 \leq k \leq \dfrac{p-1}2$. We have
\begin{align*}
kq = q_k p + r_k, \,\,\, 1 \leq r_k \leq p-1
\end{align*}
where the numbers $r_k$ are the $a_i$'s and $b_j$'s of the theorem proved previously. Also, note that $q_k = \left[\dfrac{kq}p\right]$. Let us denote $\displaystyle\sum_{j=1}^l a_j = a$, $\displaystyle\sum_{k=1}^m b_k = b$ and $\displaystyle\sum_{j=1}^{(p-1)/2} q_j = \displaystyle\sum_{j=1}^{(p-1)/2} \left[\dfrac{jq}p\right] = Q$. We hence have
$$\sum_{k=1}^{(p-1)/2} r_k = a+b$$
Also, recall that as proved earlier we have $\left\{a_1,a_2,\ldots,a_l,(p-b_1),(p-b_2), \ldots,(p-b_m)\right\} = \left\{1,2,\ldots,\dfrac{p-1}2 \right\}$. Hence, we have
\begin{align*}
1+2+\cdots+\dfrac{p-1}2 = \sum_{i=1}^l a_i + \sum_{j=1}^m (p-b_j) \implies \dfrac{p^2-1}8 = a + mp-b \tag{$\heartsuit$}
\end{align*}
We also obtain
\begin{align*}
\sum_{k=1}^{(p-1)/2} kq = \sum_{k=1}^{(p-1)/2} q_k p + \sum_{k=1}^{(p-1)/2}r_k\implies \dfrac{q(p^2-1)}8 = pQ + a+b \tag{$\diamondsuit$}
\end{align*}
Using $(\heartsuit)$ and $(\diamondsuit)$, we get
$$\dfrac{(p^2-1)(q-1)}8 = pQ + 2b-mp$$
Hence,
\begin{align*}
\dfrac{(p^2-1)(q-1)}8 \equiv_2 pQ +m \tag{$\ddagger$}
\end{align*}
\begin{itemize}
\item
Note that so far in the argument, we have not used the fact that $q>2$, i.e., $\ddagger$ still holds for $q=2$. Setting $q=2$ in $\ddagger$ gives us the second lemma of quadratic reciprocity.
$$\dfrac{p^2-1}8 \equiv_2 pQ +m$$
where $pQ = p\displaystyle\sum_{k=1}^{(p-1)/2} \left[\dfrac{2k}{p}\right] = 2+4+\cdots+(p-1) \equiv_2 0$. Hence,
$$\dfrac{p^2-1}8 \equiv_2 m$$
\item Now let us look at the case $q>2$. Since $p$ and $q$ are both odd, we have $\dfrac{(p^2-1)(q-1)}8 \equiv_2 0$. Hence, we obtain $m \equiv_2 Q$, since $p$ is odd. Using Gauss lemma, we obtain what we want, i.e.,
$$\left(\dfrac{q}p\right) = (-1)^Q$$
\end{itemize}


\hrulefill
\item
We will look at one more tool, which will enable us to prove quadratic reciprocity.
\ibox{Consider $m,n \in \mathbb{Z}^+$, such that $\gcd(m,n)=1$ and let $m_1, n_1 \in \mathbb{Z}^+$ such that $0 < m_1 < m$ and $0 < n_1 < n$. We then have
$$\sum_{i=1}^{m_1} \left[\dfrac{ni}{m}\right] + \sum_{j=1}^{n_1} \left[\dfrac{mj}{n}\right] = m_1n_1$$
}{6in}
\textbf{Proof}: Consider the following set;
$$S = \{mj-ni \vert i \in \{1,2,\ldots,m_1\}, j \in \{1,2,\ldots,n_1\}\}$$
All these numbers are distinct and none of them is zero. (Why? This is due to the fact that $\gcd(m,n) = 1$). Hence, there are a total of $\boxed{m_1n_1}$ numbers.
\begin{itemize}
\item
Let us count the number of positive integers of the form $mj-ni$. For them to be positive, we need $mj-ni>0 \implies i < \dfrac{mj}n$. Hence, for each $j$, we can run $i$ from $1$ to $\left[ \dfrac{mj}n\right]$, i.e., a total of $\left[ \dfrac{mj}n\right]$ numbers and make sure that $mj-ni$ is positive. Hence, the total number of positive numbers is $\boxed{\displaystyle \sum_{j=1}^{n_1} \left[ \dfrac{mj}n\right]}$.
\item
By the same argument, the number of negative integers of the form $mj-ni$ is $\boxed{\displaystyle \sum_{i=1}^{m_1} \left[ \dfrac{ni}m\right]}$.
\end{itemize}
We hence have
$$\boxed{\displaystyle \sum_{i=1}^{m_1} \left[ \dfrac{ni}m\right] + \displaystyle \sum_{j=1}^{n_1} \left[ \dfrac{mj}n\right] = m_1 n_1}$$

\hrulefill
\item
\textbf{Quadratic Reciprocity Theorem}: For distinct odd primes $p$ and $q$, one of the congruences $x^2\equiv p\pmod{q}$ and $y^2 \equiv q\pmod{p}$ is solvable and other other is unsolvable if $p \equiv q \equiv3\pmod4$; else both are solvable or unsolvable.\\
\textbf{Proof}:
\tbox{Quadratic Reciprocity Theorem}{Let $p, q$ be distinct odd primes. We then have
$$\left(\dfrac{p}q\right) \left(\dfrac{q}p\right) = (-1)^{\dfrac{p-1}2\cdot\dfrac{q-1}2}$$}{3.25in}
The proof follows immediately from Eisenstein's lemma. We have
$$\left(\dfrac{q}p\right) \left(\dfrac{p}q\right) = (-1)^{\displaystyle\sum_{i=1}^{(p-1)/2} \left[\dfrac{qi}p\right] +\displaystyle\sum_{j=1}^{(q-1)/2} \left[\dfrac{pj}q\right] }$$
And from the previous result, we have $\displaystyle\sum_{i=1}^{(p-1)/2} \left[\dfrac{qi}p\right] +\displaystyle\sum_{j=1}^{(q-1)/2} \left[\dfrac{pj}q\right] = \dfrac{p-1}2 \cdot \dfrac{q-1}2$. This proves the quadratic reciprocity theorem.

\item
\textbf{Application of quadratic reciprocity}:
\begin{itemize}
\item
Let us evaluate $\dfrac5p$. First observe that $$\left(\dfrac{p}5\right) = \begin{cases} 1 & \text{ if }p \equiv \pm 1\pmod5\\ -1 & \text{ if }p \equiv \pm2 \pmod5\end{cases}$$
Now note that $\dfrac{5-1}2 = 2$. Hence, by quadratic reciprocity, we have $\left(\dfrac{p}5\right)\left(\dfrac5p\right) = (-1)^{p-1} = 1$. Hence, we obtain
\ibox{$$\left(\dfrac5p\right) = \begin{cases} 1 & \text{ if }p \equiv \pm 1\pmod5\\ -1 & \text{ if }p \equiv \pm2 \pmod5\end{cases}$$}{2.5in}
\item
Similarly, let us evaluate $\left(\dfrac7p\right)$. First observe that $$\left(\dfrac{p}7\right) = \begin{cases} 1 & \text{ if }p \equiv 1,2,4\pmod7\\ -1 & \text{ if }p \equiv 3,5,6 \pmod7\end{cases}$$ Noting that $\dfrac{7-1}2 = 3$ and using quadratic reciprocity, we get that
\begin{align*}
\left(\dfrac7p\right) =
\begin{cases}
\left(\dfrac{p}7\right) & \text{ if }p \equiv 1\pmod4\\
-\left(\dfrac{p}7\right) & \text{ if }p \equiv 3\pmod4
\end{cases}
\end{align*}
Hence, we get that
\ibox{
\begin{align*}
\left(\dfrac7p\right)=
\begin{cases}
1 & \text{ if } p \equiv 1,3,9,19,25,27 \pmod{28}\\
-1 & \text{ if } p \equiv 5,11,13,15,17,23 \pmod{28}
\end{cases}
\end{align*}
}{3.5in}
\end{itemize}
In general, let us assume that $q$ is much smaller than $p$ making it possible to evaluate $\left(\dfrac{p}q\right)$ with relative ease. We then have that (by the same argument for $5$ and $7$)
\ibox{
$\left(\dfrac{q}p\right)$ is determined by $\begin{cases} p \pmod{q} & \text{ if }q \equiv1\pmod4\\ p \pmod{4q} & \text{ if }q\equiv-1\pmod4 \end{cases}$
}{4in}
\ibox{If $q\equiv1\pmod4$ is a prime and $p,p+kq$ are primes, then $\left(\dfrac{q}p\right) = \left(\dfrac{q}{p+kq}\right)$
}{5.25in}
\ibox{If $q\equiv3\pmod4$ is a prime and $p,p+4kq$ are primes, then $\left(\dfrac{q}p\right) = \left(\dfrac{q}{p+4kq}\right)$
}{5.25in}


\hrulefill
\item
Let $l \in \mathbb{Z}^+$ and $\gcd(n,2)=1$. Then
\ibox{The number of solutions of $x^2 \equiv n\pmod{2^l}$ is
\begin{itemize}
\item
$1$ if $l = 1$ and $n \equiv_2 1$.
\item
$2$ if $l=2$ and $n \equiv_4 1$.
\item
$0$ if $l=2$ and $n \equiv_4 3$.
\item
$4$ if $l\geq3$ and $n\equiv_8 1$.
\item
$0$ if $l\geq3$ and $n\equiv_8 3,5,7$.
\end{itemize}
}{3.25in}
\text{Proof}:
\begin{itemize}
\item
If $l=1$ and $n \equiv_2 1$, then $x \equiv_2 1$ is the solution.
\item
If $l=2$ and $n \equiv_4 1$, then $x \equiv_4 1,3$ are solutions.
\item
If $l=2$ and $n \equiv_4 3$, then no $x$ exists, since $x^2 \equiv_4 0,1$.
\item
If $l=3$ and $n \equiv_8 1$, then for all $x \equiv_8 \pm 1, \pm 3$, we have $x^2 \equiv_8 1$. Hence, $4$ solutions exists.
\item
If $l=3$ and $n \not\equiv_8 1$, then no solutions from above.
\item
If $l>3$, then clearly $n \not\equiv_8 1$ has no solutions, since this would imply solutions exist for $l=3$.
\item
If $l>3$ and $n \equiv_8 1$, we then want to prove that $x^2 \equiv (8k+1) \pmod{2^l}$ has exactly $4$ solutions for $k \in \{0,1,\ldots,2^{l-3}-1\}$, i.e., there exists distinct $x_1,x_2,x_3,x_4 \in \{1,3,\ldots,2^l-1\}$, such that $$x_1^2 \pmod{2^l} \equiv x_2^2 \pmod{2^l} \equiv x_3^2 \pmod{2^l} \equiv x_4^2 \pmod{2^l} \equiv (8k+1) \pmod{2^l}$$

\begin{itemize}
\item
First we will show that there are at most $4$ solutions. Note that if $y_1^2 \equiv y_2^2 \pmod{2^l}$, where $y_1,y_2 \in \{1,3,5\ldots,2^l-1\}$, we then have $2^l \mid (y_1+y_2)(y_1-y_2) \implies 2^{l-2} \mid \dfrac{y_1+y_2}{2} \cdot \dfrac{y_1-y_2}2$. Since $y_1,y_2$ are both odd, this means either $\dfrac{y_1+y_2}{2}$ or $\dfrac{y_1-y_2}{2}$ is odd. Hence, $2^{l-1}$ divides either $y_1+y_2$ xor $y_1-y_2$. Hence, $y_1^2 \equiv y_2^2 \pmod{2^l} \implies y_1 \equiv \pm y_2 \pmod{2^{l-1}}$. Now note that $$a \equiv b \pmod{c} \iff a \equiv b,b+c \pmod{2c}$$
Hence,
$$y_1^2 \equiv y_2^2 \pmod{2^l} \implies y_1 \equiv \pm y_2 \pmod{2^{l-1}} \iff y_1 \equiv \pm y_2 +2^{l-1} \pmod{2^{l}}$$
Hence, if there exists one solution to $x^2 \equiv n\pmod{2^l}$, where $\gcd(n,2)=1$, given by $x \equiv x_1\pmod{2^l}$, then there exists four solutions given by $(\pm x_1+2^{l-1})\pmod{2^l}$. Hence there are at most $4$ solutions.
\item
Now we will prove that there has to be exactly $4$ solutions for all $n$ such that $\gcd(n,2)=1$. First note that there exists $2^{l-1}$ odd numbers $x$ between $0$ and $2^l$. Consider $n \equiv_8 1$, we have $2^{l-3}$ such $n$'s between $0$ and $2^l$. Let $\chi_n$ denote the number of odd values of $x$ such that $x^2 \equiv n\pmod{2^l}$. Since each $n$ corresponds to at most $4$ distinct odd values of $x$ such that $x^2 \equiv n\pmod{2^l}$, we have $\chi_n \leq 4$. Further, from the previous part, all the odd $x$ in $0$ to $2^l$, when squared should be of the form $n\pmod{2^l}$, where $n \equiv 1\pmod8$. Hence, we have $\sum_{n=1,9}^{2^{l}-7}\chi_n$
, i.e., if we look at all $n$'s, these would correspond to $2^{l-1}$ distinct odd values of $x$ between $0$ and $2^l$. Hence, by pigeon hole principle, each $n$ must correspond to exactly $4$ values of distinct odd values of $x$ between $0$ and $2^l$.
\end{itemize}

If we have two values of $x \in \{1,3,5,\ldots,2^{l}-1\}$ such that $x_1^2 \equiv x_2^2\pmod{2^l}$, we then have $2^{l} \mid (x_1+x_2)(x_1-x_2)$. Let $x_1 = 2k_1 + 1$ and $x_2 = 2k_2+1$, where $k_1,k_2 \in \{0,1,2,\ldots,2^{l-1}-1\}$. Hence, we need $k_1,k_2 \in \{0,1,2,\ldots,2^{l-1}-1\}$ such that $2^{l-2} \mid (k_1-k_2)(k_1+k_2+1)$. Now $k_1-k_2$ and $k_1+k_2+1$ are of opposite parity. Hence, $2^{l-2}$ divides one xor the other. If $2^{l-2} \mid (k_1-k_2)$, if we fix $k_2<k_1$, since $k_1 \in \{0,1,\ldots,2^{l-1}-1\}$, we have $k_1 = k_2$ or $k_1 = 2^{l-2} + k_2$. Hence, $2$ options. Similarly, $2^{l-2} \mid (k_1+k_2+1)$ also gives us $2$ options. Hence, there are at-most $4$ solutions.

This immediately implies that there has to be exactly $4$. Note that $k$ can go from $0$ to $2^{l-3}-1$. If there exists one $k$ that has less than $4$ solutions, then this means the number of odd values of $x$ between $1$ and $2^l$ is $<4(2^{l-3}) = 2^{l-1}$. However, between $0$ and $2^l$, there are $2^{l-1}$ odd values of $x$. Hence, it suffices to prove that there is at-most $4$ solutions for each value of $k$.
\end{itemize}

\hrulefill
\item
Let $l \in \mathbb{Z}^+$ and $p \nmid n$ be an odd prime. Then
\ibox{The number of solutions of $x^2 \equiv n\pmod{p^l}$ is given by $1+\left(\dfrac{n}p\right)$}{4.25in}
\textbf{Proof}:
\begin{itemize}
\item
Let $\left(\dfrac{n}p\right) = -1$, i.e., $y^2 \equiv n\mod{p}$ has no solution for $y$. Note that $x^2 \equiv n\pmod{p^l} \implies x^2\equiv n \pmod{p}$. Hence, no solution for $x$.
\item
Let $\left(\dfrac{n}p\right) = 1$, i.e., $y^2 \equiv n\mod{p}$ has two solutions for $y$. Before we proceed, let us prove that there can be only at-most two solutions, i.e., if $x_1^2 \equiv x_2^2\pmod{p^l}$, and $(x_1,p) = 1$, $(x_2,p)=1$, we then have $x_1 \equiv \pm x_2\pmod{p^l}$. Note that $p^l \mid (x_1^2-x_2^2) \implies p^l \mid (x_1-x_2)(x_1+x_2)$. If $p^a$, where $0 < a <l$, is the highest power of $a$ that divides $x_1-x_2$, we then have that $p^{l-a} \mid x_1+x_2$. This implies $p \mid x_1$ and $p \mid x_2$, which contradicts $(x_1,p) = 1$, $(x_2,p)=1$. Hence, $ p^l \mid (x_1-x_2)$ xor $p^l \mid (x_1+x_2)$. Hence, there can be at most only two solutions, i.e., $x_1^2 \equiv x_2^2 \pmod{p^l} \iff x_1 \equiv \pm x_2\pmod{p^l}$. Now we shall prove that if $x^2$
\end{itemize}
\end{enumerate}

\end{document}