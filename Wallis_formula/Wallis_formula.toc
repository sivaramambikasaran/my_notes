\contentsline {section}{\tocsection {}{1}{Using infinite product representation of the sine function}}{1}{section.1}
\contentsline {section}{\tocsection {}{2}{Using $\displaystyle \DOTSI \intop \ilimits@ _0^{\pi /2} \qopname \relax o{sin}^n(x)$}}{1}{section.2}
\contentsline {section}{\tocsection {}{3}{Asymptotic of the central binomial coefficient}}{2}{section.3}
