\documentclass[11pt]{amsart}
\usepackage{fullpage}                % See geometry.pdf to learn the layout options. There are lots.

\usepackage{graphicx}
\usepackage{amssymb, amsmath}
\usepackage{epstopdf}

\usepackage{draftwatermark}


\SetWatermarkText{SIVARAM}
\SetWatermarkLightness{0.975}
\SetWatermarkScale{4}

\DeclareGraphicsRule{.tif}{png}{.png}{`convert #1 `dirname #1`/`basename #1 .tif`.png}

\title{Pell's equation: Ramanujan \& Mahalanobis}
%\date{}                                           % Activate to display a given date or no date

\begin{document}
\maketitle
%\section{}
%\subsection{}
This article discusses a problem posed by Mahalanobis to Ramanujan, when the former visited the latter. A short summary of linear Diophantine equation in two variables and Pell's equation is provided. The goal of this exposition is to present an overview of the more general setting the problem falls in to the general public.

\section{Problem}
There is a street with houses marked $1$ through $n$. There is a house in-between say $(x)$ such that the sum of the house numbers to left of it equals the sum of the house numbers to its right. If $n$ is between $50$ and $500$, what are $n$ and $x$?

\section{Solution}
An elementary argument gives us the following equation:
$$\underbrace{1+2+\cdots+(x-1)}^{\text{Sum of houses to the left of $x$}} = \underbrace{(x+1)+(x+2) + \cdots + (n-1) + n}^{\text{Sum of houses to the right of $x$}}$$
$$\dfrac{x(x-1)}{2} = \dfrac{n(n+1)}{2} - \dfrac{x(x+1)}{2}$$
Rearranging, we get that
\begin{align}
(n^2+n) - 2x^2 = 0 \tag{$\star$}
\end{align}
The answer to $(\star)$ satisfying the constraint $50 \leq n \leq 500$ is $$n = 288 \text{ and }x=204$$
We will in-fact give all the solutions without the constraint $50 \leq n \leq 500$.

\section{Background}
Equations such as $(\star)$ fall under the category of what are known as {Pell's equation}. ({John Pell}, a contemporary of Newton, was an English mathematician.) A more general class of equations are called {Diophantine equation}. ({Diophantus of Alexandria} was an ancient Greek mathematician who lived in the third century.) Diophantine equation is a polynomial equation in two or more variables and we allow only integer solutions. For instance, finding the integer solutions for $3x+4y = 5$ is a classic example of a linear Diophantine equation in two variables. The general solution to the above equation is given as $(3+4n,-1-3n)$ where $n$ is any integer.

\section{Linear Diophantine equation in two variables}
We can in fact give all the solutions for linear Diophantine equation in two variables. Consider the equation $ax+by = c$, where $a,b,c$ are integers. We are interested in integer solutions i.e. $(x,y)$ satisfying the equation and $x,y$ being integers. Note that a necessary condition for the equation to have solutions is that $\gcd(a,b)$ divides $c$. For example, $2x+4y = 3$ does not have integer solutions, since $2$ divides $2x+4y$ but $2$ does not divide $3$. Hence, in general, we need $\gcd(a,b)$ to divide $c$ to \emph{at-least} expect integer solutions. The non-trivial part is that the condition that $\gcd(a,b)$ divides $c$ also turns out to be sufficient. This is sometimes termed as B\'{e}zout's identity (or) B\'{e}zout's lemma. B\'{e}zout's was a French mathematician and a contemporary of Euler. For instance, consider the equation $2x+4y = 208$. This equation \emph{does have} infinite integer solutions by B\'{e}zout's lemma, since $\gcd(2,4) = 2$ divides $208$. In fact, for the equation $ax+by = c$, if $\gcd(a,b)$ divides $c$, if we guess one solution say $(x_g,y_g)$, the rest of the solutions are given by $$\left(x_g + n \dfrac{b}{\gcd(a,b)}, y_g - n \dfrac{a}{\gcd(a,b)} \right)$$where $n$ is an integer. To give a concrete example, lets look at couple of examples.

Find all integer solutions of the following:
\begin{enumerate}
\item
$9x+12y = 16$\\
This equation doesn't admit any integer solution. This is because $9x+12y = 3(3x+4y)$. But $3$ doesn't divide $16$. Hence, no integers $(x,y)$ satisfy this equation. (Note that this follows from the necessary condition we stated above i.e. $\gcd(9,12) = 3$ doesn't divide $16$.
\item
$12x+20y = 28$\\
Note that $\gcd(12,20) = 4$ divides $28$. Hence, by the sufficient condition we stated above, i.e., by B\'{e}zout's lemma, there exists infinite pairs of integer $(x,y)$ satisfying the above equation. The question now is how to find such pairs. Note that $12x+20y = 28$ can be re-written as $3x+5y = 7 (\dagger)$. Hence, we want to find all integer solutions satisfying $(\dagger)$. Note that in this case, it is \emph{easy} to guess one solution namely $(-1,2)$ satisfies the equation. Using this, we can \emph{generate} all possible solutions from the previous paragraph. Hence, $$(x,y) = \left( -1 + n \dfrac{20}{\gcd(12,20)}, 2 - n \dfrac{12}{\gcd(12,20)}\right) = \left( -1 + 5n, 2 - 3n\right)$$
\end{enumerate}
Lets wrap linear Diophantine equation in two variables here. It is not hard to extend the same idea for linear Diophantine equation in more than two variables.

\section{Pell's equation}
In general though, the degree of the Diophantine equation can be arbitrary and the number of variables can also be arbitrary in number. Pell's equation form a special class of Diophantine equation. It is quadratic equation in two variables as shown in $(\perp)$.
\begin{align*}
x^2 - Ny^2 = 1 \tag{$\perp$}
\end{align*}
where $N$ is a non-square integer, i.e., $N$ can be say $2,3,5,6,7,8,10,11,12,13,14,15,17,\ldots$. (\emph{Note that if $N$ is a square, the only solutions are the trivial solutions: $(1,0)$ and $(-1,0)$ (Why?).}) Though this equation is named Pell's equation, such equations were first studied by the Indian mathematician, Brahmagupta who lived around $6$ A.D. We will in-fact use his method named ``Chakravala method" to solve the original problem i.e. the problem posed by Mahalanobis to Ramanujan. The sanskrit name ``Chakravala" stands for ``cyclic". The ``Chakravala method" hinges on the following identity $$(x_1^2 - Ny_1^2)(x_2^2 - Ny_2^2 ) = (x_1x_2 + Ny_1 y_2)^2 - N(x_1y_2 + x_2y_1)^2.$$ The name ``Chakravala" is due to the following reason: If we start with two numbers of the form $x^2-Ny^2$ and multiply it out, we get back another number of the form $x^2 - Ny^2$. Hence, if $(x_1,y_1)$ and $(x_2,y_2)$ satisfy $x^2 - Ny^2 = 1$ i.e. if we have $x_1^2 - Ny_1^2 = 1$ and $x_2^2 - Ny_2^2 = 1$, then multiplying the two out and rearranging, we get that $$(x_1^2 - Ny_1^2)(x_2^2 - Ny_2^2 ) = (x_1x_2 + Ny_1 y_2)^2 - N(x_1y_2 + x_2y_1)^2 = 1$$ This gives us a way to generate more solutions since $(x_1x_2 + Ny_1 y_2,x_1y_2 + x_2y_1)$ now satisfies $x^2 - Ny^2$. In fact, if we know that $(x_1,y_1)$ satisfy $x_1^2 - Ny_1^2 = 1$, then $$(x_1^2 - Ny_1^2)(x_1^2 - Ny_1^2) = 1 \implies (x_1^2 + Ny_1^2)^2 - N(2x_1y_1)^2 = 1$$In fact, more can be said; If we choose $(x_1,y_1)$ \emph{appropriately} satisfying $x_1^2 - Ny_1^2 =1$, then all the solutions can be obtained as $$\left( \dfrac{\left(x_1 + \sqrt{N} y_1 \right)^n+\left( x_1 - \sqrt{N} y_1 \right)^n}{2} , \dfrac{\left(x_1 + \sqrt{N} y_1 \right)^n-\left( x_1 - \sqrt{N} y_1 \right)^n}{2\sqrt{N}} \right)$$ where $n$ is any positive integer. Note that $n=1$ gives us the original solution $(x_1,y_1)$. Let us see this in action.
\section{Example}
\noindent
Find all positive integer solutions for $x^2 - 2y^2 =1$.

This example is, as we will see at the end, in fact the same as the problem $(\star)$ we began with. First note that $(3,2)$ satisfies this problem since $3^2 -2 \times 2^2 = 1$. This will be the generator now. (\emph{This generator infact generates all positive integer solutions}.) So now lets see how we generate solutions. 
We have
\begin{align*}
3^2 - 2 \times 2^2 & = 1\\
(3+ 2 \sqrt{2}) (3 - 2 \sqrt{2}) & = 1\\
\end{align*}
Now lets square both sides and proceed.
\begin{align*}
(3+ 2 \sqrt{2})^2 (3 - 2 \sqrt{2})^2 & = 1\\
(9 + 8 + 12 \sqrt{2}) (9 + 8 - 12 \sqrt{2}) & = 1\\
(17 + 12\sqrt{2}) (17 - 12\sqrt{2}) & = 1\\
17^2 - 2 \times 12^2 & = 1
\end{align*}
Hence, now we find that $(17,12)$ satisfies the equation $x^2 -2y^2 = 1$. Now lets find one more solution based on the same method and the general idea should be clear. As before, we have $(3+ 2 \sqrt{2}) (3 - 2 \sqrt{2}) =1$.
Now lets cube both sides and proceed.
\begin{align*}
(3+ 2 \sqrt{2})^3 (3 - 2 \sqrt{2})^3 & = 1^3\\
(3^3 + 3 \times 3^2 \times 2 \sqrt{2} + 3 \times 3 \times (2 \sqrt{2})^2 + (2 \sqrt{2})^3) (3^3 - 3 \times 3^2 \times 2 \sqrt{2} + 3 \times 3 \times (2 \sqrt{2})^2 - (2 \sqrt{2})^3) & = 1\\
(27 + 54 \sqrt{2} + 72 + 16 \sqrt{2}) (27 - 54 \sqrt{2} + 72 - 16 \sqrt{2}) & = 1\\
(99 + 70 \sqrt{2}) (99 - 70 \sqrt{2}) & = 1\\
99^2 - 2 \times 70^2 & = 1
\end{align*}
Hence, in general, we raise the equation $(3+ 2 \sqrt{2}) (3 - 2 \sqrt{2}) =1$ to the $n^{th}$ power on both sides to get the general solution. The general solution can be compactly written as
$$(x,y) = \left( \dfrac{\left(3 + 2\sqrt{2} \right)^n+\left( 3 - 2\sqrt{2} \right)^n}{2} , \dfrac{\left(3 + 2\sqrt{2} \right)^n-\left( 3 - 2\sqrt{2} \right)^n}{2\sqrt{2}} \right)$$ where $n$ is any positive integer.\\
$n=1$ gives us $(x,y) = \left( \dfrac{\left(3 + 2\sqrt{2} \right)^1+\left( 3 - 2\sqrt{2} \right)^1}{2} , \dfrac{\left(3 + 2\sqrt{2} \right)^1-\left( 3 - 2\sqrt{2} \right)^1}{2\sqrt{2}} \right) = \left( 3,2\right)$.\\
$n=2$ gives us $(x,y) = \left( \dfrac{\left(3 + 2\sqrt{2} \right)^2+\left( 3 - 2\sqrt{2} \right)^2}{2} , \dfrac{\left(3 + 2\sqrt{2} \right)^2-\left( 3 - 2\sqrt{2} \right)^2}{2\sqrt{2}} \right) = \left( 17,12\right)$.\\
$n=3$ gives us $(x,y) = \left( \dfrac{\left(3 + 2\sqrt{2} \right)^3+\left( 3 - 2\sqrt{2} \right)^3}{2} , \dfrac{\left(3 + 2\sqrt{2} \right)^3-\left( 3 - 2\sqrt{2} \right)^3}{2\sqrt{2}} \right) = \left( 99,70\right)$.\\
$n=4$ gives us $(x,y) = \left( \dfrac{\left(3 + 2\sqrt{2} \right)^4+\left( 3 - 2\sqrt{2} \right)^4}{2} , \dfrac{\left(3 + 2\sqrt{2} \right)^4-\left( 3 - 2\sqrt{2} \right)^4}{2\sqrt{2}} \right) = \left( 577, 408\right)$.\\
$n=5$ gives us $(x,y) = \left( \dfrac{\left(3 + 2\sqrt{2} \right)^5+\left( 3 - 2\sqrt{2} \right)^5}{2} , \dfrac{\left(3 + 2\sqrt{2} \right)^5-\left( 3 - 2\sqrt{2} \right)^5}{2\sqrt{2}} \right) = \left( 3363, 2378\right)$.\\
and so on...

\section{Getting back to the original problem}
Let us now see how this is related to our original problem $(\star)$. The original problem led us to the equation $(n^2 + n) -2 x^2 = 0$. Multiplying by $4$ gives us $(4n^2 + 4n) - 8x^2 = 0$. Adding $1$ gives us $$(4n^2 + 4n + 1) - 2(2x)^2 = 1 \implies (2n+1)^2 - 2(2x)^2 = 1$$ Calling $(2n+1)$ as $X$ and $2x$ as $Y$, we get that $X^2 - 2Y^2 = 1$. This was precisely the problem we solved in the previous section. Hence, we can enlist all the solutions. If we enforce the constraint that $50 \leq n \leq 500$, then in-terms of $X$, the constraint becomes $101 \leq X \leq 1001$ (since $X = 2n+1$). The only solution in the above list such that $101 \leq X \leq 1001$ is $(X,Y) = (577,408)$. This gives us that $n = \dfrac{X-1}{2} = \dfrac{576}{2} = 288$. $2x = Y = 408 \implies x = 204$. Hence, the answer to the question is $$n = 288 \text{ and }x=204$$ i.e. to say explicitly the sum of numbers from $1$ to $203$ is the same as the sum of numbers from $205$ to $288$.
\end{document}  