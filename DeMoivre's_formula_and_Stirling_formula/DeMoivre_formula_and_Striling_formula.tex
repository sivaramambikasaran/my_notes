\documentclass{amsart}   	% use "amsart" instead of "article" for AMSLaTeX format
\usepackage{fullpage}
\usepackage{amsmath, amssymb} 
\usepackage{draftwatermark}
\usepackage{hyperref}

\SetWatermarkText{SIVARAM}
\SetWatermarkLightness{0.975}
\SetWatermarkScale{4}
\setcounter{tocdepth}{1}

\newcommand{\dint}{\displaystyle \int}


\usepackage{tcolorbox}

\newcommand{\tbox}[3]{
\begin{center}
\begin{tcolorbox}[title=#1,width=#3,coltext=red,colback=green!15!white,colframe=blue!80!black]
#2
\end{tcolorbox}
\end{center}
}

\newcommand{\ibox}[2]{
\begin{center}
\begin{tcolorbox}[width=#2,coltext=red,colback=green!15!white,colframe=blue!80!black]
#1
\end{tcolorbox}
\end{center}
}

\title{De Moivre's formula and Stirling's formula}
\author{Sivaram Ambikasaran}
\date{}							% Activate to display a given date or no date

\begin{document}
\maketitle
%\tableofcontents

\hrulefill

The article looks at a proof of $n! \sim \sqrt{2 \pi n} \left(\dfrac{n}e \right)^n$. De Moivre first proved that $n! \sim C \sqrt{n} \left(\dfrac{n}e \right)^n$. Stirling subsequently proved that $C = \sqrt{2 \pi}$.

\hrulefill

\section{De Moivre's formula}

\tbox{De Moivre's formula}{$$n! \sim {C}{\sqrt{n}} \left(\dfrac{n}e \right)^n$$}{1.75in}
There are many ways of obtaining this formula. In this article, we will rely on Abel partial summation technique to derive this formula.

\subsection{Abel partial summation technique}

The broad idea behind Abel partial summation technique is that it allows us to obtain information about a summation of the form $\sum_{k \leq n} a_k b_k$, if we know the summation $\sum_{k \leq n} a_k = A_n$. This can be seen as follows:
\begin{align*}
\sum_{k=1}^n a_k b_k & = \sum_{k=1}^n (A_{k} - A_{k-1}) b_k = \sum_{k=1}^n A_k b_k - \sum_{k=1}^n A_{k-1} b_k\\
& = \sum_{k=1}^n A_k b_k - \sum_{k=0}^{n-1} A_{k} b_{k+1} = A_n b_n - A_0 b_1 - \sum_{k=1}^{n-1} A_k (b_{k+1} - b_k)
\end{align*}

\emph{Note that the above is nothing but the discrete version of integration by parts, i.e.,} $$\dint_{1^-}^{n^+} b_x dA_x = \left. A_x b_x \right \vert_{1^-}^{n^+} - \dint_{1^+}^{n^-} A_x db_x$$

In our case, we let $a_x = 1$ and $b_x = \log(x)$. We then have $A_x = \lfloor x \rfloor$. Hence, we obtain

\begin{align*}
\sum_{k = 1}^n \log(k) & = n \log(n) - 0 - \dint_{1^+}^{n^-} \lfloor x \rfloor \dfrac{dx}x = n \log(n) - \dint_{1^+}^{n^-} \dfrac{x-\{x\}}x dx = n \log(n) - (n-1) + \int_{1^+}^{n^-}\dfrac{\{x\}}x dx\\
& = n \log(n) - (n-1) + \int_{1^+}^{n^-}\dfrac{\{x\}-1/2}x dx + \int_{1^+}^{n^-}\dfrac{dx}{2x} = n \log(n) - (n-1) + \int_{1^+}^{n^-}\dfrac{\{x\}-1/2}x dx + \dfrac{\log(n)}2\\
& = \left(n+\dfrac12\right) \log(n) - n +1 + \int_{1^+}^{n^-}\dfrac{\{x\}-1/2}x dx \tag{$\star$}
\end{align*}
Let $B(t) = t-1/2$ for $t \in [0,1]$. Define $\dfrac{ dB_1(t)}{dt} = B(t)$, such that $\dint_0^1 B_1(t) dt = 0$. We have $B_1(t) = \dfrac{t^2-t}2 + C$. $C$ is obtained by setting $\dint_0^1 \dfrac{t^2-t}2 dt + C = 0 \implies C + \dfrac{1/3-1/2}2= 0 \implies C = \dfrac1{12}$. We hence have

\begin{align*}
\int_{1^+}^{n^-}\dfrac{\{x\}-1/2}x dx = \int_{1^+}^{n^-} \dfrac{dB_1(\{x\})}x = \int_{1^+}^{n^-} \dfrac{B_1(\{x\})}{x^2} dx = \int_{1^+}^{\infty} \dfrac{B_1(\{x\})}{x^2} dx - \int_{n^-}^{\infty} \dfrac{B_1(\{x\})}{x^2} dx \tag{$\spadesuit$}
\end{align*}

Plugging $(\spadesuit)$ in $(\star)$, we get
\begin{align*}
\sum_{k = 1}^n \log(k) = \left(n+\dfrac12\right) \log(n) - n +1 + \int_{1^+}^{\infty} \dfrac{B_1(\{x\})}{x^2} dx - \int_{n^-}^{\infty} \dfrac{B_1(\{x\})}{x^2} dx \tag{$\dagger$}
\end{align*}

Now note that we have $\left \vert B_1(\{x\})\right \vert \leq K$, where $K = \dfrac1{12}$. Hence,
\begin{align*}
\left \vert -\dint_{n^-}^{\infty} \dfrac{B_1(\{x\})}{x^2} dx \right \vert \leq \dint_{n^-}^{\infty} \dfrac{\left \vert B_1(\{x\}) \right \vert}{x^2} dx \leq \dint_{n^-}^{\infty} \dfrac{K dx}{x^2} = \dfrac{K}n \tag{$\clubsuit$}
\end{align*}

\begin{align*}
\left \vert \dint_{1^+}^{\infty} \dfrac{B_1(\{x\})}{x^2} dx \right \vert \leq \dint_{1^+}^{\infty} \dfrac{\left \vert B_1(\{x\}) \right \vert}{x^2} dx \leq \dint_{1^+}^{\infty} \dfrac{K dx}{x^2} = K \tag{$\heartsuit$}
\end{align*}

Using $(\clubsuit)$ and $(\heartsuit)$ in $(\dagger)$, we get that

\begin{align*}
\sum_{k = 1}^n \log(k) = \left(n+\dfrac12\right) \log(n) - n + \text{constant} + \mathcal{O}(1/n) = \left(n+\dfrac12\right) \log(n) - n + \log(C) + \mathcal{O}(1/n) \tag{$\perp$}
\end{align*}

Rewriting $(\perp)$, we get that
\begin{align*}
n! = C \sqrt{n} \left(\dfrac{n}e \right)^n e^{\mathcal{O}(1/n)} \sim C \sqrt{n} \left(\dfrac{n}e \right)^n \tag{$\diamondsuit$}
\end{align*}

\hrulefill

\section{Stirling's formula}
\tbox{Stirling's formula}{$$n! \sim {\sqrt{2 \pi n}} \left(\dfrac{n}e \right)^n$$}{1.5in}
To obtain Stirling's formula, we can use the asymptotic for the central binomial coefficients obtained using the Wallis's formula discussed in \href{https://www.dropbox.com/s/y6cxeq6rwamdgik/Wallis_formula.pdf}{this article}. We have
\begin{align*}
\dbinom{2n}n \sim \dfrac{4^n}{\sqrt{\pi n}} \tag{$\ddagger$}
\end{align*}
From De Moivre's formula derived, we also have
\begin{align*}
\dbinom{2n}n = \dfrac{(2n)!}{n! n!} \sim \dfrac{C \sqrt{2n} \left(\dfrac{2n}e\right)^{2n}}{C^2 n \left(\dfrac{n}e \right)^{2n}} = \dfrac{4^n \sqrt2}{C\sqrt{n}} \tag{$\pm$}
\end{align*}
Comparing $\pm$ and $\ddagger$, we get $C = \sqrt{2\pi}$. It is also to be noted that computing $C$ is related to computing the error integral discussed in \href{https://www.dropbox.com/s/zhz1qfzljn4y53b/Error_Integral.pdf}{this article}.

\hrulefill

\end{document}