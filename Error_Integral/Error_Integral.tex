\documentclass{amsart}
\usepackage{amsmath, amssymb} 
%\usepackage{draftwatermark}
\usepackage{fullpage}


\usepackage{draftwatermark}


\SetWatermarkText{SIVARAM}
\SetWatermarkLightness{0.975}
\SetWatermarkScale{4}


\newcommand{\Rb}{\mathbb{R}}
\newcommand{\dint}{\displaystyle \int}
\newcommand{\doint}{\displaystyle \oint}
\newcommand{\dsum}{\displaystyle \sum}
%\SetWatermarkText{SIVARAM}
%\SetWatermarkLightness{0.875}
%\SetWatermarkScale{5}
\setcounter{tocdepth}{1}
\title{Different ways of proving $\dint_{\Rb} e^{-x^2}dx = \sqrt{\pi}$.}
\author{Sivaram Ambikasaran}

\begin{document}
\maketitle
\tableofcontents
This article looks at different ways of proving $\dint_{\Rb} e^{-x^2}dx = \sqrt{\pi}$. Most of them are well-known, couple of them I devised on my own, only to figure out later that the method was already well-known. Most of the arguments presented here are rigorous, while the rest can be made rigorous.
%No credit is given to anyone for the proofs for the following two reasons: (i) It is hard to exactly figure out who came up with it first. (ii) Since I believe art is more important than the artist, this doesn't matter.

It is to be noted that by a change of variable, namely $x^2=t$, it is easy to show that $\dint_{\Rb} e^{-x^2}dx = \Gamma(1/2)$, which will be of use in a few proofs. Also, throughout the article, $I$ denotes $\dint_{\Rb} e^{-x^2} dx$. None of the proofs presented are circular, i.e., the premise for each proof is independent of the fact that $I = \sqrt{\pi} $.

\hrulefill

\section{Moving to polar coordinates}
We then have $I^2 = \dint_{\Rb} e^{-x^2} dx \dint_{\Rb} e^{-y^2} dy = \dint_{\Rb^2} e^{-(x^2+y^2)} dx dy$. Now let us perform a change of variables (move to polar coordinates); $x = r \cos(t)$, $y = r \sin(t)$, where $r \in [0, \infty)$ and $\theta = [0, 2\pi)$. We have $dx dy = r dr dt$. Hence,
\begin{align*}
I^2 & = \dint_{\Rb^2} e^{-(x^2+y^2)} dx dy = \int_{0}^{2 \pi} \int_0^{\infty} e^{-r^2} r dr dt = \left(\int_0^{\infty} e^{-r^2} r dr \right) \left( \int_{0}^{2 \pi} dt \right)\\
& = \dfrac12 \left(\int_0^{\infty} e^{-r^2} d(r^2) \right) \left( \int_{0}^{2 \pi} dt \right) = \dfrac12 \left(\int_0^{\infty} d \left(-e^{-r^2} \right) \right) \left( 2 \pi \right) = \pi
\end{align*}
Hence, $\boxed{I = \sqrt{\pi}}$

\hrulefill

\section{Moving to spherical coordinates}
Another method similar to above. We have
$$I^3 = \dint_{\Rb} e^{-x^2} dx \dint_{\Rb} e^{-y^2} dy \int_{\Rb} e^{-z^2} dz = \dint_{\Rb^3} e^{-x^2-y^2-z^2} dx dy dz$$
Now lets move to the spherical polar coordinates. We then have $x = r \sin(\theta) \cos(\phi)$, $y = r \sin(\theta) \sin(\phi)$ and $z = r \cos(\theta)$. We then have $dx dy dz = r^2 \sin(\theta) dr d\theta d \phi$. Hence, $I^3 = \dint_{0}^{2\pi} \dint_0^{\pi} \dint_0^{\infty} e^{-r^2} r^2 \sin(\theta) dr d\theta d\phi$. Hence,
\begin{align*}
I^3 = \left( \dint_0^{\infty} e^{-r^2} r^2 dr\right) \left(\dint_0^{\pi} \sin(\theta) d \theta\right) \left(\dint_0^{2\pi} d \phi\right) = \left( \dint_0^{\infty} e^{-r^2} r^2 dr\right) \times 2 \times (2\pi) = 2 \pi \dint_{\Rb} r^2 e^{-r^2} dr \tag{$\star$}
\end{align*}
By integration by parts, we have
\begin{align*}
\dint_{\Rb} r^2 e^{-r^2} dr = -\dfrac12\dint_{\Rb} r d \left(e^{-r^2}\right) = - \dfrac12 \left. r e^{-r^2}\right \vert_{-\infty}^{\infty} + \dfrac12\int_{\mathbb{R}} e^{-r^2} dr = 0 + \dfrac{I}2 \tag{$\dagger$}
\end{align*}
Using $(\dagger)$ in $(\star)$, we get
$$I^3 = 2 \pi \times \dfrac{I}2 = \pi I$$
Clearly, $I > 0$. Hence, we get $\boxed{I = \sqrt{\pi}}$

\hrulefill

\section{Change of variable technique}
We have $I = \dint_{\Rb} e^{-x^2}dx = 2 \int_0^{\infty} e^{-x^2}dx$. As before, we have
$$I^2 = 4 \int_{[0, \infty)^2} e^{-x^2-y^2} dy dx$$
Now let $y = sx$, and since $y$ goes from $0$ to $\infty$, so does $s$ for any fixed $x$. Hence, we have
$$I^2 = 4 \int_{[0, \infty)^2} x e^{-(1+s^2) x^2} ds dx = 4 \int_{[0, \infty)^2} x e^{-(1+s^2) x^2} dx ds$$
We now have
$$\int_{0}^{\infty} x e^{-(1+s^2) x^2} dx = \underbrace{\dfrac12 \int_{0}^{\infty} 2x e^{-(1+s^2) x^2} dx = \dfrac12 \int_{0}^{\infty} e^{-(1+s^2) t} dt}_{x^2 = t} = \dfrac12 \left. \dfrac{e^{-(1+s^2)t}}{-(1+s^2)} \right \vert_{0}^{\infty} = \dfrac12 \dfrac1{1+s^2}$$
Hence,
$$I^2 = 4 \int_0^{\infty} \dfrac{ds}{2(1+s^2)} = \int_0^{\infty} \dfrac{2 ds}{1+s^2} = 2 \left. \arctan(s) \right \vert_0^{\infty} = 2 \cdot \dfrac{\pi}2 = \pi \implies \boxed{I = \sqrt{\pi}}$$

\hrulefill

\section{Solid of revolution}

As with the first proof, we have
\begin{align*}
I^2 & = \dint_{\Rb^2} e^{-(x^2+y^2)}dx dy \tag{$\star$}
\end{align*}
Now if we look at a solid object, whose surface is given by $z=e^{-(x^2+y^2)}$, where $x,y \in \Rb$, then $(\star)$ computes the volume of the object, i.e., we have
$$V = \dint_{\Rb^2} z dx dy$$
The volume of the object can also be computed using the method of discs. So starting at $z=0$, we chop the solid into an infinite set of discs for all values between $z=0$ and $z=1$. To do this, the radius of the disc at height $z$, is given by $r(z) = \sqrt{-\ln(z)}$, where $z \in (,1]$.
$$V = \int_{0}^1 \pi r^2(z) dz = \int_{0}^1 \pi (-\ln(z)) dz = -\pi \left( z \ln(z) - z\right )_{z=0}^{z=1} = \pi \implies \boxed{I = \sqrt{\pi}}$$
\hrulefill

\section{Functional equation of $\Gamma$-function}

Let $x = \sqrt{t}$, we get $dx = \dfrac{dt}{2\sqrt{t}}$. Hence,
\begin{align*}
I = 2\int_0^{\infty} e^{-x^2} dx = 2\int_0^{\infty} e^{-t} \dfrac{dt}{2 \sqrt{t}} = \int_0^{\infty} t^{1/2 - 1} e^{-t} dt = \Gamma(1/2) & \tag{$\star$}
\end{align*}
A one line introduction to $\Gamma$ function. For $t \in \Rb_{>0}^+$, $\Gamma(t)$ is defined as $\Gamma(t) = \dint_{\Rb^+} e^{-x} x^{t-1} dx$. The function is then extended over the entire complex plane, except at $0$ and negative integers, by analytic continuation.

Now the $\Gamma$ function satisfies the functional equation given by
\begin{align*}
\Gamma(z) \Gamma(1-z) = \dfrac{\pi}{\sin(\pi z)} & \tag{$\dagger$}
\end{align*}
Setting $z = 1/2$ in the functional equation $(\dagger)$, we get $\Gamma(1/2)^2 = \dfrac{\pi}{\sin(\pi/2)} = \pi \implies \Gamma(1/2) = \sqrt{\pi}$. Plugging this into $(\star)$, we get $\boxed{I = \sqrt{\pi}}$

\hrulefill

\section{Beta function}

Here is another method of evaluating $\Gamma(1/2)$ and thereby obtain $I = \sqrt{\pi}$. We have the beta function
\begin{align*}
\beta(x,y) & = \int_0^1 t^{x-1} (1-t)^{y-1} dt \tag{$\star$}
\end{align*}
We also have the relation between the $\beta$ and $\Gamma$ function given as
\begin{align*}
\beta(x,y) & = \dfrac{\Gamma(x+y)}{\Gamma(x) \Gamma(y)} \tag{$\dagger$}
\end{align*}
From $(\dagger)$, setting $x=y=\dfrac12$ and noting that $\Gamma(1) = 0! = 1$, we get
\begin{align*}
\Gamma(1/2)^2 & = \beta(1/2,1/2) = \int_0^1 t^{-1/2} (1-t)^{-1/2} dt \tag{$\spadesuit$}
\end{align*}
Setting $t = \sin^2(\theta)$ in above, we get that
\begin{align*}
\Gamma(1/2)^2 & = \beta(1/2,1/2) = \int_0^{\pi/2} \dfrac{2\sin(\theta) \cos(\theta)}{\sin(\theta) \cos(\theta)} d\theta = \pi\tag{$\clubsuit$}
\end{align*}
which again gives us $\Gamma(1/2) = \sqrt{\pi}$ and therefore $\boxed{I = \sqrt{\pi}}$

\hrulefill

\section{Multiplication identity of $\Gamma$ function: Method $1$}

Here is another method of evaluating $\Gamma(1/2)$ using the multiplication identity of $\Gamma$-function. We have
\begin{align*}
\prod_{k=0}^{m-1} \Gamma\left(z + \dfrac{k}m\right) = (2 \pi)^{(m-1)/2} m^{1/2-mz} \Gamma(mz) \tag{$\dagger$}
\end{align*}
Setting $m=2$ in $(\dagger)$ gives us the duplication identity for the $\Gamma$ function, which is
\begin{align*}
\Gamma(z) \Gamma(z+1/2) = 2^{1-2z} \sqrt{\pi} \Gamma(2z) \tag{$\star$}
\end{align*}
Setting $z=1/2$ in $(\star)$ gives us
\begin{align*}
\Gamma(1/2) \Gamma(1) = 2^{0} \sqrt{\pi} \Gamma(1) \implies \Gamma(1/2) = \sqrt{\pi} \implies \boxed{I = \sqrt{\pi}}
\end{align*}

\hrulefill

\section{Multiplication identity of $\Gamma$ function: Method $2$}

Using the multiplicative identity and setting $m=2$, as in the previous case we have
\begin{align*}
\Gamma(z) \Gamma(z+1/2) = 2^{1-2z} \sqrt{\pi} \Gamma(2z) \tag{$\star$}
\end{align*}
Rewriting $(\star)$ gives us
\begin{align*}
\Gamma(z+1/2) = 2^{1-2z} \sqrt{\pi} \dfrac{\Gamma(2z)}{\Gamma(z)} \tag{$\clubsuit$}
\end{align*}
Now recall that for $z$ close to $0$, we have $\Gamma(z) = \dfrac{\Gamma(z+1)}z \sim \dfrac1z$. Hence, taking limit of $(\clubsuit)$ gives us
\begin{align*}
\Gamma(1/2) = \lim_{z \to 0}\Gamma(z+1/2) = 2 \sqrt{\pi} \dfrac{1/2}{1} = \sqrt{\pi} \implies \boxed{I = \sqrt{\pi}}
\end{align*}

\hrulefill

\section{Functional equation of $\zeta$-function: Method $1$}

A one line introduction to the $\zeta$-function. For $s \in \Rb_{>1}^+$, $\zeta(s)$ is defined as $\zeta(s) = \dsum_{n=1}^{\infty} \dfrac1{n^s}$. The function is then extended over the entire complex plane, except at $1$, by analytic continuation. The $\zeta$-function satisfies the functional equation
\begin{align*}
\zeta(s) & = 2^s \pi^{s-1} \sin\left(\dfrac{\pi s}2\right) \Gamma(1-s) \zeta(1-s)
\end{align*}
Setting $s=1/2$, we get
\begin{align*}
\zeta(1/2) & = \sqrt2 \sqrt{1/\pi} \sin\left(\dfrac{\pi}4\right) \Gamma(1/2) \zeta(1/2) \implies \Gamma(1/2) = \sqrt{\pi} \implies \boxed{I = \sqrt{\pi}}
\end{align*}

\hrulefill

\section{Functional equation of $\zeta$-function: Method $2$}

Riemann also obtained a symmetric version of the function relation, which can be rewritten as
\begin{align*}
\pi^{-s/2} \Gamma(s/2) \zeta(s) = \pi^{-(1-s)/2} \Gamma((1-s)/2) \zeta(1-s)
\end{align*}
Note that as $s \to 1$, $\zeta(s) \to \infty$ and so does $\Gamma((1-s)/2)$. However, we have for $s$ in the vicinity of $1$, $\zeta(s-1) \sim \dfrac1{s-1}$ and $\Gamma((1-s)/2) \sim \dfrac2{1-s}$. Hence, taking the limit as $s \to 1$, we get
$$\pi^{-1/2} \Gamma(1/2) = {\zeta(0)} \implies \Gamma(1/2) = -2\zeta(0) \sqrt{\pi}$$ Hence, it suffice to prove that $\zeta(0) = -\dfrac12$. This fact is shown in another post of mine. Hence, we again obtain $\boxed{I = \sqrt{\pi}}$

\hrulefill

\section{Wallis's formula and dominated convergence theorem}

\subsection*{Wallis's formula}
$$\prod_{n=1}^{\infty} \left( \dfrac{2n}{2n-1} \cdot \dfrac{2n}{2n+1} \right) = \dfrac{\pi}2$$
Using the Wallis' formula, we can obtain the asymptotic of the central binomial coefficient as
$$\dbinom{2n}n \sim \dfrac{4^n}{\sqrt{\pi n}}$$

\subsection*{Dominated convergence theorem}
The version we need is the following: If $\{f_n\}$ are a sequence of integrable real-valued function on $\mathbb{R}$, such that $f_n \to f$ point-wise and $\vert f_n \vert \leq f(x)$ for all $n$ and for all $x \in D$, where $\dint_{\Rb} f dx < \infty$, then $$\dint_{\Rb} f_n dx \to \dint_{\Rb} f dx$$

Now consider the sequence of functions defined as follows:
$$f_n(x) =
\begin{cases}
\left(1- \dfrac{x^2}n \right)^n & \text{if }\vert x \vert \leq \sqrt{n}\\
0 & \text{if }\vert x \vert > \sqrt n
\end{cases}
$$
and we have $f(x) = e^{-x^2}$. It is easy to check that $f_n \to f$ and $\vert f_n(x) \vert \leq f$ for all $n$ and $x$. Hence, we have $\dint_{\Rb} e^{-x^2} dx = \dint_{\Rb} f_n dx$. Let us now look at evaluating $\dint_{\Rb} f_n dx$. We have
$$\dint_{\Rb} f_n dx = \overbrace{\int_{\vert x \vert \leq \sqrt{n}} \left(1-\dfrac{x^2}n\right)^n dx = \sqrt{n}\int_{\vert t \vert \leq \pi/2} \cos^{2n+1}(t) dt}^{x = \sqrt{n} \sin(t)} = 2 \sqrt{n} J_{2n+1}$$
We have $J_{2n+1} = \underbrace{\dfrac{4^n}{2n+1} \dfrac1{\dbinom{2n}n} \sim \dfrac{\sqrt{\pi n}}{2n+1}}_{\text{Wallis's formula}}$. Hence, we have
$$\dint_{\Rb} f_n dx \sim \dfrac{2n}{2n+1} \sqrt{\pi} \implies \boxed{I = \sqrt{\pi}}$$

\hrulefill

\section{De Moivre's formula}

\subsection*{De Moivre's formula} $$\Gamma(z) \sim \dfrac{C}{\sqrt{z}} \left(\dfrac{z}e\right)^z$$
The task of computing $C$ is equivalent to computing $\Gamma(1/2)$. With $C = \sqrt{2\pi}$ the asymptotic is popularly known as Stirling's formula. However, for the current proof, we do not need to know the value of $C$. In fact, we can use the method below to compute $C$ as $\sqrt{2 \pi}$.

Also, recall that $\left(1-\dfrac{a}n\right)^{n-k} \sim e^{-a}$
Now let us put all these together; We have
\begin{align*}
\Gamma\left(n+\dfrac12\right) = \left(n-\dfrac12\right)\left(n-\dfrac32\right) \cdots \dfrac12 \Gamma(1/2) = \underbrace{\dfrac{(2n)!}{4^n n!} \Gamma(1/2) \sim \dfrac{n!}{\sqrt{\pi n}} \Gamma(1/2)}_{\text{Wallis's formula}} \implies \dfrac{\Gamma(n+1/2)}{\Gamma(n+1)} \sim \dfrac{\Gamma(1/2)}{\sqrt{\pi n}} \tag{$\diamondsuit$}
\end{align*}
Using De Moivre, we get
\begin{align*}
\dfrac{\Gamma(n+1/2)}{\Gamma(n+1)} \sim \dfrac{\sqrt{n+1}}{\sqrt{n+1/2}}\left(\dfrac{n+1/2}e\right)^{n+1/2} \cdot \left(\dfrac{e}{n+1} \right)^{n+1} = \dfrac{\sqrt{e}}{\sqrt{n+\dfrac1{2}}} \dfrac{(n+1/2)^{n+1/2}}{(n+1)^{n+1/2}} \sim \dfrac1{\sqrt{n}} \tag{$\heartsuit$}
\end{align*}
Comparing $(\diamondsuit)$ and $(\heartsuit)$, we get $\Gamma(1/2) = \sqrt{\pi} \implies \boxed{I = \sqrt{\pi}}$

\hrulefill

\section{Differentiating and swapping integrals}
Consider $I(s) = \dint_{-s}^s e^{-x^2} dx$ and $J(s) = I^2(s)$. We then have
\begin{align*}
J'(s) = 2I(s) I'(s) = 4I(s) e^{-s^2} = \underbrace{4e^{-s^2} \dint_{-s}^s e^{-x^2}dx = 4e^{-s^2} \dint_{-1}^1 s e^{-s^2 y^2}dy}_{x = sy} = 4\int_{-1}^1 se^{-s^2(1+y^2)}dy \tag{$\star$}
\end{align*}
Note that $J(0) = 0$. Hence, integrating $(\star)$, we get that
\begin{align*}
\lim_{t \to \infty} J(t) & = 4\int_0^{\infty} \int_{-1}^1 se^{-s^2(1+y^2)}dyds = 4\int_{-1}^1 \int_0^{\infty} se^{-s^2(1+y^2)}dsdy = 2\int_{-1}^1 \int_0^{\infty} e^{-s^2(1+y^2)}d\left({s^2} \right)dy\\
& = 2 \int_{-1}^1 \left(\left. \dfrac{-e^{-s^2(1+y^2)}}{1+y^2} \right \vert_{s=0}^{s=\infty} \right) dy = 2 \int_{-1}^1 \dfrac{dy}{1+y^2} = 2 \left. \arctan(y) \right \vert_{y=-1}^{y=1} = 2\left(\dfrac{\pi}2\right) = \pi
\end{align*}
Hence, we have $\boxed{I = \sqrt{\pi}}$

\hrulefill

\section{Bounding the integrand based on convexity of $e^{-x}$}

Since $e^{-x}$ is convex, we have for $x>0$, $$1-x^2 \leq e^{-x^2} = \dfrac1{e^{x^2}} \leq \dfrac1{1+x^2} \implies (1-x^2)^{n^2} \leq e^{-n^2x^2} \leq \dfrac1{(1+x^2)^{n^2}}$$ Hence,
$$\dint_0^1(1-x^2)^{n^2} dx \leq \dint_0^1 e^{-n^2x^2} dx \leq \dint_0^1 \dfrac{dx}{(1+x^2)^{n^2}}$$
Let us now isolate each of the integrals:
\begin{enumerate}
\item
$\overbrace{\dint_0^1(1-x^2)^{n^2} dx = \int_0^{\pi/2} \cos^{2n^2+1}(t) dt}^{x = \sin(t)} = I_{2n^2+1}$.
\item
$\dint_0^1 e^{-n^2x^2} dx = \dfrac1n \int_0^n e^{-y^2} dy$
\item
$\overbrace{\dint_0^1 \dfrac{dx}{(1+x^2)^{n^2}} = \dint_0^{\pi/4} \dfrac{\sec^2(t) dt}{\sec^{2n^2}(t)}}_{x = \tan(t)} = \dint_0^{\pi/4} \cos^{2n^2-2}(t)dt < \dint_0^{\pi/2} \cos^{2n^2-2}(t)dt = I_{2n^2-2}$.
\end{enumerate}
We have the integral
$$
\boxed{
I_k = \dint_0^{\pi/2} \cos^k(t) dt = 
\begin{cases} 
\dfrac{\pi}{2^{2m+1}} \dbinom{2m}m & \text{ if }k=2m\\
\dfrac{2^{2m}}{2m+1} \dfrac1{\dbinom{2m}m} & \text{ if }k=2m+1
\end{cases}
}
$$
Hence, putting all this together, we get that
\begin{align*}
\overbrace{\dfrac{2^{2n^2}}{2n^2+1} \dfrac{n}{\dbinom{2n^2}{n^2}}}^{a_n} \leq \dint_0^n e^{-y^2} dy < \overbrace{\dfrac{n\pi}{2^{2n^2-1}} \dbinom{2n^2-2}{n^2-1}}^{b_n}
\end{align*}
Hence, if we show that $\lim_{n \to \infty} a_n = \lim_{n \to \infty} b_n = \dfrac{\sqrt{\pi}}2$, we are done; but this immediately follows from Wallis's formula. Hence, we get that $\boxed{I = \sqrt{\pi}}$

\hrulefill

\section{Fourier transform: Poisson Summation formula}

Here we will work with the integral $\dint_{\Rb} e^{-\pi x^2} dx$ instead of $\dint e^{-x^2} dx$. This is to avoid some unpleasant scaling in the Fourier transform and Poisson summation formula.

We will use the following Fourier transform:
$$\widehat{f(x)}(y) = \dint_{\Rb} f(x) e^{-2\pi iyx}dx$$
$$f(x) = \dint_{\Rb} \widehat{f(x)}(y) e^{2 \pi i yx} dy$$
A few useful properties for functions in the Schwartz class:
\begin{enumerate}
\item
\begin{align*}
\widehat{f'(x)}(y) & = \dint_{\Rb} f'(x) e^{-2 \pi iyx}dx\\
& = \underbrace{\dint_{\Rb} e^{-2 \pi iyx}d(f(x)) = 2 \pi iy \dint_{\Rb} e^{-2 \pi iyx} f(x) dx}_{\text{Integration parts and then assume the function is nice enough}}\\
& = 2 \pi iy \widehat{f(x)}(y)
\end{align*}

\item
\begin{align*}
\widehat{f(x)}'(y) & = \dint_{\Rb} -2\pi ix f(x) e^{-2\pi iyx}dx = -2\pi i \widehat{(xf(x))}(y)
\end{align*}

\item
The Gaussian $e^{-\pi x^2}$ is the unique solution to
\begin{align*}
h'(x) + 2 \pi x h(x) = 0 \tag{$\star$}
\end{align*}
with $h(0) = 1$.
\end{enumerate}

Taking the Fourier transform of $(\star)$ and making use of the properties of Fourier transform, we get
\begin{align*}
\widehat{h'(x)}(y) + 2 \pi \widehat{x h(x)} = 0\\
2 \pi i y \widehat{h(x)}(y) + 2 \pi \dfrac{\widehat{h(x)}'(y)}{-2\pi i} = 0\\
\widehat{h(x)}'(y) + 2\pi y \widehat{h(x)}(y) = 0
\end{align*}

Hence, we have
\begin{align}
\widehat{h(x)}(y) = c h(y) \tag{$\dagger$}
\end{align}

We shall denote the gaussian $e^{-\pi x^2}$ as $h(x)$ for the remainder of the proof.

\subsection{Poisson Summation formula}
For any nice function $g(x)$ (Schwartz class), we have
$$\sum_{k = -\infty}^{\infty} g(k) = \sum_{k=-\infty}^{\infty} \widehat{g(x)}(k)$$
Plugging in $g(x) = h(x) = e^{-\pi x^2}$, we get that
$$\sum_{k = -\infty}^{\infty} h(k) = \sum_{k=-\infty}^{\infty} \widehat{h(x)}(k) \implies \sum_{k = -\infty}^{\infty} h(k) = c\left(\sum_{k=-\infty}^{\infty} h(k) \right)\implies c = 1$$

We hence have
\begin{align}
\widehat{h(x)}(y) = h(y) \tag{$\dagger$}
\end{align}

Setting $y=0$ in $(\dagger)$, we get that
$$1 = h(0 ) = \widehat{h(x)}(0) = \dint_{\Rb} e^{-\pi x^2} dx \implies \boxed{I = \sqrt{\pi}}$$

\hrulefill

\section{Contour integration}

Consider the function $f(z) = \dfrac{e^{-z^2}}{1+e^{-2z\sqrt{a}}}$, where $a = i\pi$. Now note that $$f(z+\sqrt{a}) = \dfrac{e^{-(z+\sqrt{a})^2}}{1+e^{-2(z+\sqrt{a})\sqrt{a}}} = \dfrac{e^{-(z+\sqrt{a})^2}}{1+e^{-2z\sqrt{a}}} = \dfrac{e^{-(z^2+a+2\sqrt{a}z)}}{1+e^{-2z\sqrt{a}}} = -\dfrac{e^{-(z^2+2\sqrt{a}z)}}{1+e^{-2z\sqrt{a}}}$$
We have $f(z+\sqrt{a}) - f(z) = -\left(\dfrac{e^{-(z^2+2\sqrt{a}z)}+e^{-z^2}}{1+e^{-2z\sqrt{a}}}\right) = -e^{-z^2}$. The poles of $f$ are at $-2z\sqrt{a} = i \pi (2n+1)$, i.e., at $z = \sqrt{a}(n+1/2)$. Consider a rectangular contour in the complex plane enclosing just one pole at $z=\sqrt{a}/2$, i.e., let $t=\text{Imag}(\sqrt{a})$ and consider the rectangle, $\partial \Omega_s$, with vertices $[-s,0]$, $[s,0]$, $[s,t]$ and $[-s,t]$. We then have
\begin{align*}
\doint_{\partial \Omega_s} f(z) dz & = 2 \pi i \lim_{z \to \sqrt{a}/2} \left(\dfrac{z-\sqrt{a}/2}{1+e^{-2z \sqrt{a}}} e^{-z^2} \right) = 2 \pi i e^{-a/4} \dfrac1{-2\sqrt{a}e^{-a}} = -\dfrac{\pi i}{\sqrt{a}}e^{3a/4} = -\dfrac{\pi i e^{3i \pi/4}}{\sqrt{i \pi}}\\
& = - \sqrt{i \pi}e^{3i \pi/4} = -\sqrt{\pi} \sqrt{e^{i \pi/2}} e^{3i\pi/4} = -\sqrt{\pi} e^{i \pi} = \sqrt{\pi}
\end{align*}
Letting $s \to \infty$, we have the integral over $[s,0]$ to $[s,t]$ to be to zero. Similarly, we have the integral over $[-s,t]$ to $[-s,0]$ tends to zero as $s \to \infty$. Hence, we get that
\begin{align*}
\sqrt{\pi} = \int_{-\infty}^{\infty} f(z) dz + \int_{\infty + it}^{-\infty + it} f(z) dz = \int_{-\infty}^{\infty} f(z) dz + \int_{\infty}^{-\infty} f(z + it) dz = \int_{-\infty}^{\infty} f(z) dz - \int_{-\infty}^{\infty} f(z + it) dz \tag{$\spadesuit$}
\end{align*}
But we also have
\begin{align*}
\int_{-\infty}^{\infty} f(z) dz - \int_{-\infty}^{\infty} f(z+it) dz = \int_{-\infty}^{\infty} \left(f(z) - f(z+\sqrt{a}) \right)dz = \int_{-\infty}^{\infty} e^{-z^2}dz \tag{$\clubsuit$}
\end{align*}
Comparing $(\spadesuit)$ and $(\clubsuit)$, we get $\boxed{I = \sqrt{\pi}}$


\hrulefill

\end{document}