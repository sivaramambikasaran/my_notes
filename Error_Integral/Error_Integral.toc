\contentsline {section}{\tocsection {}{1}{Moving to polar coordinates}}{1}
\contentsline {section}{\tocsection {}{2}{Moving to spherical coordinates}}{2}
\contentsline {section}{\tocsection {}{3}{Change of variable technique}}{2}
\contentsline {section}{\tocsection {}{4}{Solid of revolution}}{2}
\contentsline {section}{\tocsection {}{5}{Functional equation of $\Gamma $-function}}{3}
\contentsline {section}{\tocsection {}{6}{Beta function}}{3}
\contentsline {section}{\tocsection {}{7}{Multiplication identity of $\Gamma $ function: Method $1$}}{4}
\contentsline {section}{\tocsection {}{8}{Multiplication identity of $\Gamma $ function: Method $2$}}{4}
\contentsline {section}{\tocsection {}{9}{Functional equation of $\zeta $-function: Method $1$}}{4}
\contentsline {section}{\tocsection {}{10}{Functional equation of $\zeta $-function: Method $2$}}{4}
\contentsline {section}{\tocsection {}{11}{Wallis's formula and dominated convergence theorem}}{5}
\contentsline {subsection}{\tocsubsection {}{}{Wallis's formula}}{5}
\contentsline {subsection}{\tocsubsection {}{}{Dominated convergence theorem}}{5}
\contentsline {section}{\tocsection {}{12}{De Moivre's formula}}{5}
\contentsline {subsection}{\tocsubsection {}{}{De Moivre's formula}}{5}
\contentsline {section}{\tocsection {}{13}{Differentiating and swapping integrals}}{6}
\contentsline {section}{\tocsection {}{14}{Bounding the integrand based on convexity of $e^{-x}$}}{6}
\contentsline {section}{\tocsection {}{15}{Fourier transform: Poisson Summation formula}}{7}
\contentsline {subsection}{\tocsubsection {}{15.1}{Poisson Summation formula}}{8}
\contentsline {section}{\tocsection {}{16}{Contour integration}}{8}
