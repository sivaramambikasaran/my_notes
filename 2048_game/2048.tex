\documentclass{article}
\usepackage{hyperref}
\usepackage{amsmath,amssymb}
\usepackage{tikz}
\usepackage{fullpage}
\usepackage{draftwatermark}
\usepackage{subfigure}

\SetWatermarkText{SIVARAM}
\SetWatermarkLightness{0.975}
\SetWatermarkScale{4}

\title{$2048$ game}
\date{}
\author{Sivaram Ambikasaran}
\begin{document}
\maketitle

Those who have played/won the \href{http://gabrielecirulli.github.io/2048/}{$2048$ game} will soon realize that the $2048$ tile is by no means the upper bound. An average player with reasonably sufficient time can easily reach $4096$ tile (Figure~\ref{figure_4096}). So the question is ``What is the upper bound of the maximum tile?'' The answer to this is evident for those who have developed a strategy to play the game. (It is worth recalling that the key fact that the game exploits is $2^k + 2^k = 2^{k+1}$.) One of the popular strategies is to form the maximum tile at one of the four corners of the board. A loose motivation behind this strategy is that fixing the tile at one of the corners gives us two degrees of freedom (most of the time) under which the maximum tile won't move, thus making it easier to focus on the rest of the board. Any other location for the maximum tile will result in moving the maximum tile more frequently. Once we have our maximum tile, say $M$, at some corner, the strategy then is to populate the board going either row-wise or column-wise with tiles of value $\dfrac{M}2$, $\dfrac{M}4$ and so on in a snake like fashion (for instance, as shown in Figure~\ref{figure_strategy} for a $4 \times 4$ case, with the maximum tile being formed at the top-left and subsequent tiles being arrange row-wise in a clock-wise fashion).

\begin{figure}[!htbp]
\begin{center}
\subfigure[An occasion of $4096$ appearing]{
\includegraphics[scale=0.1]{./reaching_4096_For_Second_Time.png}
\label{figure_4096}
}
\subfigure[A possible strategy]{
\begin{tikzpicture}[scale=1]
\draw (0,0) grid (4,4);
\node at (0.5,3.75) {$M$};
\node at (1.5,3.75) {$M/2$};
\node at (2.5,3.75) {$M/2^2$};
\node at (3.5,3.75) {$M/2^3$};
\draw [<-] (0.75,3.5) -- (3.25,3.5);
\draw [domain=-90:90] plot ({3.25+0.5*cos(\x)}, {3+0.5*sin(\x)});
\node at (3.5,2.25) {$M/2^4$};
\node at (2.5,2.25) {$M/2^5$};
\node at (1.5,2.25) {$M/2^6$};
\node at (0.5,2.75) {$M/2^7$};
\draw [-] (0.75,2.5) -- (3.25,2.5);
\draw [domain=90:270] plot ({0.75+0.5*cos(\x)}, {2+0.5*sin(\x)});
\node at (0.5,1.25) {$M/2^8$};
\node at (1.5,1.25) {$M/2^9$};
\node at (2.5,1.25) {$M/2^{10}$};
\node at (3.5,1.75) {$M/2^{11}$};
\draw [-] (0.75,1.5) -- (3.25,1.5);
\draw [domain=-90:90] plot ({3.25+0.5*cos(\x)}, {1+0.5*sin(\x)});
\node at (3.5,0.25) {$M/2^{12}$};
\node at (2.5,0.25) {$M/2^{13}$};
\node at (1.5,0.25) {$M/2^{14}$};
\node at (0.5,0.5) {$\spadesuit$};
\draw [-] (0.75,0.5) -- (3.25,0.5);
\end{tikzpicture}
\label{figure_strategy}
}
\end{center}
\end{figure}

If the randomness were to favor us, we would like to have $\spadesuit$ to equal $M/2^{14}$ and the maximum value of $\spadesuit$ is $4$. Hence, we need $\dfrac{M}{2^{14}} = 4 \implies M = 2^{16}$. Once we have this, we have the domino effect and the maximum tile that can be obtained is $\boxed{2M = 2^{17}}$ at the top left corner.

The more general setting:
\begin{center}
Given an $\underbrace{n \times n \times \cdots n}_{d \text{ times}}$ hypercube with $2, 4, \ldots, 2^k$'s appearing at random, what is the maximum hyper-tile that can be reached?
\end{center}

By the same strategy, the answer is when $\dfrac{M}{2^{n^d-2}} = 2^k \implies M = 2^{n^d+k-2}$ and the maximum tile that can be formed is $\boxed{2^{n^d+k-1}}$. For the usual game, with $2$'s and $4$'s appearing at random, we have $n=4$, $d=2$ (hypercube in $2$D is a square) and $k=2$ ($4 = 2^2$), giving the maximum tile as $2^{17}$.

\newpage

\textbf{Maximum score and maximum number of moves on a $n \times n$ square}: The user's score starts at zero, and is incremented whenever two tiles combine, by the value of the new tile. For instance, if a user reaches tile $4$ making use of two $2$'s, then the score increments by $4$. Similarly, if a user reaches $8$ making use of only $2$'s, then the score would be $$8+2 \times \text{Reaching }4 \text{ using two two's} = 8 + 2 \times 4 = 16$$ In general, if $S(m)$ denotes the increment in score on reaching a tile $2^m$, we have
$$S(2^m) = 2^m + 2S(2^{m-1}) \text{ with } S(2^1) = 0$$ Hence, we get
$$S(2^m) = 2^m + 2^m + 2^2 S(2^{m-2}) = (m-1)2^m + 2^{m-1} S(2) = (m-1)2^m$$
Hence, in the process of reaching the tile $2^m$, the maximum increment to the score is $(m-1)2^m$. However, when we want to reach the maximum tile, we would want the randomness to throw $4$ at the bottom-left square at the critical step and we do not get a score of $4$ for forming this. Hence, the score we obtain in the process of reaching the maximum tile is $(m-1)2^m-4$. Once we have reached the maximum tile at the top-right square, to reach the maximum score the goal is to form half of this tile at the square to its immediate left and so on. Hence, the total score is
\begin{align}
\sum_{m=3}^{n^2+1} \left((m-1)2^{m-1}-4 \right) = 4 \left(2^{n^2}-1\right)\left(n^2-1\right) \tag{$\spadesuit$}
\end{align}
For the usual game played on $4 \times 4$ square, the maximum score is obtained by setting $n=4$ in $(\spadesuit)$, which gives us the maximum score of $\boxed{3,932,100}$.

By a similar argument, if $M(m)$ denotes the number of moves needed to reach a tile of $2^m$, then we have
$$M(2^m) = 1 + 2M(2^{m-1}) \text{ with } M(2) = 0$$
Solving this recurrence gives us $M(2^m) = 2^{m-1}-1$. Hence, in the process of reaching the tile $2^m$, the maximum number of moves we would have made is $2^{m-1}-1$. However, when we want to reach the maximum tile, we would want the randomness to throw $4$ at the bottom-left square at the critical step and we do not make a move to get this $4$. Hence, the number of moves made to reach the maximum tile is $2^{m-1}-2$. Once we have reached the maximum tile at the top-right square, we can still make valid moves as discussed above. Hence, the maximum number of possible moves is
\begin{align}
\sum_{m=3}^{n^2+1} \left(2^{m-1}-2 \right) = 2\left(2^{n^2}-\left(n^2+1\right)\right) \tag{$\diamondsuit$}
\end{align}
For the usual game played on $4 \times 4$ square, the maximum number of moves is obtained by setting $n=4$ in $(\diamondsuit)$, which gives us the maximum number of moves as $\boxed{131,038}$.

Similar expressions can be obtained for $n$-hypercube with $2,4,\ldots,2^k$'s appearing at random.
\end{document}