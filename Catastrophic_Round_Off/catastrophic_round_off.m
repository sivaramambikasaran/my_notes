% Filename: catastrophic_round_off.m
% This solves the following recurrence:
% a(1) = a(2) = 2.95;
% a(n+2) = 10a(n+1) - 9a(n);
% If we had infinite precision, then a(n) would be '2.95' for all n.
% Let us look at how finite precision affect this computation.
clear all;
clc;
N   =   20;			%	Number of terms
a   =   zeros(N,1);	%	Initialize all a(n)'s to be zero initially
a(1)=   2.95;       %	a(1) is set to '2.95'
a(2)=   a(1);		%	a(2) is set to '2.95'
for n=1:N-2
    a(n+2)  =   10*a(n+1)-9*a(n);	%	Recurrence: a(n+2) = 10a(n+1)-9a(n)
end
a					%	Display the first N values obtained using the computation